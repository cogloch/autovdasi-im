#include "camera.h"
#include <GLFW/glfw3.h>
#include <imgui/imgui.h>
#include <glm/gtc/matrix_transform.hpp>


Camera::Camera(glm::vec3 position, glm::vec3 up, GLfloat yaw, GLfloat pitch)
    : m_position(position)
    , m_front(0.0f, 0.0f, -1.0f)
    , m_up(), m_right()
    , m_worldUp(up)
    , m_yaw(yaw)
    , m_pitch(pitch)
    , m_movementSpeed(SPEED)
    , m_mouseSensitivity(SENSITIVTY)
    , m_zoom(ZOOM)
{
    UpdateCameraVectors();
}

Camera::Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch)
    : m_position(glm::vec3(posX, posY, posZ))
    , m_front(0.0f, 0.0f, -1.0f)
    , m_up(), m_right()
    , m_worldUp(upX, upY, upZ)
    , m_yaw(yaw)
    , m_pitch(pitch)
    , m_movementSpeed(SPEED)
    , m_mouseSensitivity(SENSITIVTY)
    , m_zoom(ZOOM)
{
    UpdateCameraVectors();
}

glm::mat4 Camera::GetViewMatrix() const
{
    return glm::lookAt(m_position, m_position + m_front, m_up);
}

void Camera::Tick(double dt)
{
    auto& io = ImGui::GetIO();
    if (io.KeysDown[GLFW_KEY_W])
        ProcessKeyboard(FORWARD, dt);
    if (io.KeysDown[GLFW_KEY_S])
        ProcessKeyboard(BACKWARD, dt);
    if (io.KeysDown[GLFW_KEY_A])
        ProcessKeyboard(LEFT, dt);
    if (io.KeysDown[GLFW_KEY_D])
        ProcessKeyboard(RIGHT, dt);

    if (io.MouseDown[1])
        ProcessMouseMovement({ io.MouseDelta.x, io.MouseDelta.y });
    ProcessMouseScroll(io.MouseWheel);
}

void Camera::ProcessKeyboard(Camera_Movement direction, GLfloat deltaTime)
{
    auto velocity = m_movementSpeed * deltaTime;

    if (direction == FORWARD)
        m_position += m_front * velocity;
    if (direction == BACKWARD)
        m_position -= m_front * velocity;
    if (direction == LEFT)
        m_position -= m_right * velocity;
    if (direction == RIGHT)
        m_position += m_right * velocity;
}

void Camera::ProcessMouseMovement(glm::vec2 offset, GLboolean constrainPitch)
{
    offset.x *= m_mouseSensitivity;
    offset.y *= m_mouseSensitivity;

    m_yaw += offset.x;
    m_pitch -= offset.y;

    // Make sure that when pitch is out of bounds, screen doesn't get flipped
    if (constrainPitch)
    {
        if (m_pitch > 89.0f)
            m_pitch = 89.0f;
        if (m_pitch < -89.0f)
            m_pitch = -89.0f;
    }

    // Update Front, Right and Up Vectors using the updated Eular angles
    UpdateCameraVectors();
}

void Camera::ProcessMouseScroll(GLfloat yoffset)
{
    if (m_zoom >= 1.0f && m_zoom <= 45.0f)
        m_zoom -= yoffset;
    if (m_zoom <= 1.0f)
        m_zoom = 1.0f;
    if (m_zoom >= 45.0f)
        m_zoom = 45.0f;
}

void Camera::UpdateCameraVectors()
{
    // Calculate the new Front vector
    glm::vec3 front;
    front.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
    front.y = sin(glm::radians(m_pitch));
    front.z = sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
    m_front = glm::normalize(front);
    // Also re-calculate the Right and Up vector
    m_right = glm::normalize(glm::cross(m_front, m_worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
    m_up = glm::normalize(glm::cross(m_right, m_front));
}