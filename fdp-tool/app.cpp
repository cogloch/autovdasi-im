﻿#include "app.h"
#include "input.h"
#include "renderer.h"
#include <string>
#include <set>
#include "ui.h"

#undef APIENTRY
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#include <glfw/glfw3native.h>

#undef min


App::App()
{
    InitWindow();
    m_pRenderer = new Renderer(m_pWindow);
	glfwSetWindowSize(m_pWindow, 1024, 768);
    InitUI();
}

void App::RunMainLoop()
{
	Wing::s_pWingMgr = &m_wingMgr;
    u32 lastAirfoil = 0;
	UIState ui; 
	ui.pAirfoilMgr = &m_airfoilMgr;
	ui.pWingMgr = &m_wingMgr;

    m_wingMgr.StartUpdating();
	
    m_timer.Reset();

    while (!glfwWindowShouldClose(m_pWindow))
    {
		Timer dbg;

        glfwPollEvents();
        m_pRenderer->NewFrame();
        
		const auto dt = m_timer.Tick() / 1000;
		input::Tick(m_pWindow);

		ui.NewFrame(dt);
		ApplyUIStateChanges(ui);
		
        m_camera.Tick(dt);

        gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
        m_pRenderer->DrawFrame(m_camera);
        ImGui::Render();
        glfwSwapBuffers(m_pWindow);

        m_wingMgr.Sync(m_airfoilMgr.GetCopy(), lastAirfoil);
		lastAirfoil = m_airfoilMgr.GetNumAirfoils();
    }
}

void App::InitWindow()
{
    if (!glfwInit())
        throw "GLFW init failed";

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    m_pWindow = glfwCreateWindow(800, 600, "fdp-tool", nullptr, nullptr);
    glfwMakeContextCurrent(m_pWindow);
    
	glfwSetWindowSizeCallback(m_pWindow, [](GLFWwindow* pWindow, int width, int height) {
		int fbWidth, fbHeight;
		glfwGetFramebufferSize(pWindow, &fbWidth, &fbHeight);

		auto& io = ImGui::GetIO();
		io.DisplaySize = { (float)width, (float)height };
		io.DisplayFramebufferScale = { width > 0 ? ((float)fbWidth / width) : 0,
			height > 0 ? ((float)fbHeight / height) : 0 };

		gl::Viewport(0, 0, fbWidth, fbHeight);
	});

	input::SetCallbacks(m_pWindow);
}

void App::InitUI()
{
    auto& io = ImGui::GetIO();
    io.KeyMap[ImGuiKey_Tab] = GLFW_KEY_TAB;
    io.KeyMap[ImGuiKey_LeftArrow] = GLFW_KEY_LEFT;
    io.KeyMap[ImGuiKey_RightArrow] = GLFW_KEY_RIGHT;
    io.KeyMap[ImGuiKey_UpArrow] = GLFW_KEY_UP;
    io.KeyMap[ImGuiKey_DownArrow] = GLFW_KEY_DOWN;
    io.KeyMap[ImGuiKey_PageUp] = GLFW_KEY_PAGE_UP;
    io.KeyMap[ImGuiKey_PageDown] = GLFW_KEY_PAGE_DOWN;
    io.KeyMap[ImGuiKey_Home] = GLFW_KEY_HOME;
    io.KeyMap[ImGuiKey_End] = GLFW_KEY_END;
    io.KeyMap[ImGuiKey_Delete] = GLFW_KEY_DELETE;
    io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
    io.KeyMap[ImGuiKey_Enter] = GLFW_KEY_ENTER;
    io.KeyMap[ImGuiKey_Escape] = GLFW_KEY_ESCAPE;
    io.KeyMap[ImGuiKey_A] = GLFW_KEY_A;
    io.KeyMap[ImGuiKey_C] = GLFW_KEY_C;
    io.KeyMap[ImGuiKey_V] = GLFW_KEY_V;
    io.KeyMap[ImGuiKey_X] = GLFW_KEY_X;
    io.KeyMap[ImGuiKey_Y] = GLFW_KEY_Y;
    io.KeyMap[ImGuiKey_Z] = GLFW_KEY_Z;

    io.SetClipboardTextFn = [](void* user_data, const char* text) { glfwSetClipboardString((GLFWwindow*)user_data, text); };
    io.GetClipboardTextFn = [](void* user_data) { return glfwGetClipboardString((GLFWwindow*)user_data); };
    io.ClipboardUserData = m_pWindow;
    io.ImeWindowHandle = glfwGetWin32Window(m_pWindow);
}

void App::ApplyUIStateChanges(UIState& ui)
{
	m_pRenderer->light.pos = { 0.f, -32.f, 0.f };
	m_pRenderer->light.color = { 1.f, 1.f, 1.f };

	if (ui.bWingConfigChanged)
	{
		m_wingMgr.GetWingRef(ui.selectedWing).GenerateMesh(&m_pRenderer->GetWingMeshRef());
	}

	if (ui.selectedWing != -1)
		//m_wingMgr.GetWingRef(ui.selectedWing) = ui.config; // TODO figure out what I was thinking 
        ui.config = m_wingMgr.GetWingRef(ui.selectedWing);

	if (ui.bSaveConfigs)
		m_wingMgr.SaveWings();

	if(ui.bSetToBestConfig)
		m_wingMgr.GetWingRef(ui.selectedWing).SetToBest();
	
	for (auto idx : ui.needsUpdating)
	{
		auto& wing = m_wingMgr.GetWingRef(idx);
		wing.CalculatePerformance(wing.rootData, wing.tipData, wing.rootGeom, wing.tipGeom, wing.tipMeta, wing.rootMeta);
	}

	if (!m_wingMgr.GetNumWings()) return;
	if (ui.selectedWing != -1)
	{
		u32 oldId = m_wingMgr.GetWingRef(ui.selectedWing).GetId();
		m_wingMgr.RankWings(ui.bRankByCurrent);
		for (u32 idx = 0; idx < m_wingMgr.GetNumWings(); ++idx)
		{
			if (m_wingMgr.GetWingRef(idx).GetId() == oldId)
			{
				ui.selectedWing = idx;
				break;
			}
		}
	}
	else
	{
		m_wingMgr.RankWings();
	}
}
