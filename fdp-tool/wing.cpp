#include "wing.h"
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <string>
#include <fstream>
#include "wing_mgr.h"


const double Ring::minThicknessThreshold = 0.005;
const double Wing::depthLimit = 30.0;
const double Wing::heightLimit = 7.5;
const float Wing::halfSpan = 35.f;
WingMgr* Wing::s_pWingMgr = nullptr;


// TODO test for different allowable values 
const Material foam = {
    33,     // 32-33
    25,     // 10% possible variation
    13.5,   // 10% possible variation 
    0.45,   // 0.45-0.50
    0.25,   // 0.25-0.30
    0.2     // 0.20-0.25
};

Wing::Wing(u32 rootAirfoil_, u32 tipAirfoil_, u32 id_)
    : id(id_)
	, bestConfigId(id_)
    , rootAirfoil(rootAirfoil_)
    , tipAirfoil(tipAirfoil_)
{
}

bool Wing::operator==(const Wing& other) const
{
    return rootMeta.name == other.rootMeta.name && tipMeta.name == other.tipMeta.name;
}

bool Wing::IsValid() const
{
    // TODO airfoil names as unique id 

    if (rootAirfoil == INVALID_AIRFOIL_ID || tipAirfoil == INVALID_AIRFOIL_ID) return false;

    return rootGeom.coords.size() == tipGeom.coords.size() && !rootGeom.coords.empty();
}

bool Wing::operator!=(const Wing& other) const 
{
    return (rootAirfoil != other.rootAirfoil) || (tipAirfoil != other.tipAirfoil) ||
           (sweep != other.sweep) || (taperRatio != other.taperRatio) || (twist != other.twist) ||
           (rootChord != other.rootChord) || (halfSpan != other.halfSpan) || (dihedral != other.dihedral);
}

bool Wing::CurrentBetter(const Wing& other) const
{
	return maxClCd > other.maxClCd;
}

bool Wing::BestBetter(const Wing& other) const
{
	return s_pWingMgr->m_bestWings[bestConfigId].maxClCd > s_pWingMgr->m_bestWings[other.bestConfigId].maxClCd;
}

// TODO transformations on existing geometry 
void Wing::GenerateGeometry()
{
    m_slices.clear();
    
    const auto& root = rootGeom.coords;
    const auto& tip = tipGeom.coords;
    
    if (root.size() != tip.size()) return;
    const u32 numPts = root.size();

    m_root.Reset(numPts);
    m_tip.Reset(numPts);
    for(u32 i = 0; i < numPts; ++i)
    {
        m_root.AddPoint(root[i].x, root[i].y, 0.f);
        m_tip.AddPoint(tip[i].x, tip[i].y, 0.f);
    }

    float cgx = 0.f, cgy = 0.f;
    for(u32 i = 0; i < numPts; ++i)
    {
        cgx += m_tip.points[i].x;
        cgy += m_tip.points[i].y;
    }
    cgx /= numPts; cgy /= numPts;

    for(u32 i = 0; i < numPts; ++i)
    {
        auto& pt = m_tip.points[i];
        float dri = sqrt((cgx - pt.x) * (cgx - pt.x) + (cgy - pt.y) * (cgy - pt.y));
        float dxi = -twist + glm::degrees(atan2(cgx - pt.x, cgy - pt.y));
    
        m_tip.points[i].x = -dri * sin(glm::radians(dxi)) + cgx;
        m_tip.points[i].y = -dri * cos(glm::radians(dxi)) + cgy;
    }

    for (u32 i = 0; i < numPts; ++i)
    {
        m_root.points[i].x *= rootChord;
        m_root.points[i].y *= rootChord;

        m_tip.points[i].x *= rootChord * taperRatio;
        m_tip.points[i].y *= rootChord * taperRatio;
    }

    m_tip.localPts = m_tip.points;
    m_root.localPts = m_root.points;

    auto shiftSweep = halfSpan * sin(glm::radians(sweep));
    auto shiftDihedral = halfSpan * sin(glm::radians(dihedral));
    for(auto& pt : m_tip.points)
    {
        pt.x += shiftSweep;
        pt.y += shiftDihedral;
        pt.z += halfSpan;
    }

    if (!m_root.Analyze() || !m_tip.Analyze()) return;
    halfVolume = halfSpan * (m_root.GetArea() + m_tip.GetArea()) / 2.0;
    
    // Volume upper limit
    // Spanwise dimension is fixed 
    double minX = std::numeric_limits<double>::max(), maxX = std::numeric_limits<double>::lowest(); // Chordwise
    double minY = std::numeric_limits<double>::max(), maxY = std::numeric_limits<double>::lowest(); // Thicknesswise 
    for (u32 i = 0; i < m_tip.points.size(); ++i)
    {
        auto tipPt = m_tip.points[i];
        auto rootPt = m_root.points[i];

        if (tipPt.x < minX) minX = tipPt.x;
        if (tipPt.x > maxX) maxX = tipPt.x;
        if (rootPt.x < minX) minX = rootPt.x;
        if (rootPt.x > maxX) maxX = rootPt.x;

        if (tipPt.y < minY) minY = tipPt.y;
        if (tipPt.y > maxY) maxY = tipPt.y;
        if (rootPt.y < minY) minY = rootPt.y;
        if (rootPt.y > maxY) maxY = rootPt.y;
    }

    if (abs(maxX - minX) > depthLimit)
        bNeedReduceDepth = true;
    else
        bNeedReduceDepth = false;

    if (abs(maxY - minY) > heightLimit)
        bNeedReduceHeight = true;
    else
        bNeedReduceHeight = false;
}

void StitchSlices(Mesh& mesh, u32 numVertsInSlice, u32 firstSliceStart, u32 secondSliceStart)
{
    for (u32 vertOffset = 0; vertOffset < numVertsInSlice - 1; vertOffset++)
    {
        mesh.SetQuadFace(firstSliceStart + vertOffset,
                         secondSliceStart + vertOffset,
                         secondSliceStart + vertOffset + 1,
                         firstSliceStart + vertOffset + 1);
    }

    mesh.SetQuadFace(secondSliceStart,
        firstSliceStart,
                     firstSliceStart + numVertsInSlice - 1,
                     secondSliceStart + numVertsInSlice - 1);
}

void Wing::GenerateMesh(Mesh* pOut)
{
    if(!IsValid())
    {
        pOut->bRenderable = false;
        return;
    }

    // TODO don't throw away the sections on each iteration 
    Mesh mesh;

    auto sliceSize = m_root.points.size();
    auto numSlices = m_slices.size() + 2; // Including root and tip sections

    mesh.verts.reserve(sliceSize * (2 + m_slices.size()));
    mesh.AppendVerts(m_root.points);
    for (auto& slice : m_slices)
        mesh.AppendVerts(slice.points);
    mesh.AppendVerts(m_tip.points);

    // Generate indices
    for (u32 i = 0; i < numSlices - 1; ++i)
        StitchSlices(mesh, sliceSize, sliceSize * i, sliceSize * (i + 1));

    // Stitch wingtips(cutoff)
    /*for (int pointIdx = 0; pointIdx < vertsPerSection - 1; ++pointIdx)
    {
        mesh.indices.push_back(pointIdx);
        mesh.indices.push_back(pointIdx + 1);
        mesh.indices.push_back(vertsPerSection - 1 - pointIdx);
    }*/

    mesh.GenerateNormals();

    mesh.CreateGraphicsResources();

    *pOut = mesh;
}

void Wing::Serialize(rapidjson::PrettyWriter<rapidjson::StringBuffer>& writer) const
{
    writer.StartObject();

    writer.String("rootAirfoil"); writer.Int(rootAirfoil);
    writer.String("tipAirfoil"); writer.Int(tipAirfoil);
    writer.String("sweep"); writer.Double(sweep);
    writer.String("taperRatio"); writer.Double(taperRatio);
    writer.String("twist"); writer.Double(twist);
    writer.String("rootChord"); writer.Double(rootChord);
    writer.String("dihedral"); writer.Double(dihedral);

    writer.EndObject();
}

void Wing::Deserialize(rapidjson::Value& serialized)
{
	sweep = serialized["sweep"].GetFloat();
	taperRatio = serialized["taperRatio"].GetFloat();
	twist = serialized["twist"].GetFloat();
	rootChord = serialized["rootChord"].GetFloat();
	dihedral = serialized["dihedral"].GetFloat();
}

void Wing::ExportToGCode(std::string& gcodePath) const
{
	const auto sweepDataLine = std::to_string(sweep) + R"(     % Sweep in degrees from leading edge(for simplicity)        sweep)" + '\n';
	const auto taperRatioDataLine = std::to_string(taperRatio) + R"(      % Taper ratio		                                          taper)" + '\n';
	const auto twistDataLine = std::to_string(twist) + R"(      % Twist in degrees, positive is tip LE down                  twist)" + '\n';
	const auto rootChordDataLine = std::to_string(rootChord) + R"( % Length of chord at root                                    chord)" + '\n';
	const auto dihedralDataLine = std::to_string(dihedral) + R"(      % Dihederal in degrees                                       dihed)" + '\n';

	const auto settingsData = R"(35       % Half span of wing (in case foam block is larger)           hSpan
115.3    % Length of hot wire (cm)                                    cuttnSpan
15       % Height of cutting arms (off table) (cm)                    cutHeight
40       % Width of cutting space (along table) (cm)                  cuttWidth
0.2      % Radius of effective cut (cm)                               cutRadius
2.0      % Distance to avoid the edge of the block by (cm)            cuttBndry
20       % Cutting rate in cm/minute                                  cuttnRate
35       % Length of foam block (cm)                                  blockSpan
30       % Width of foam block (cm)                                   blockWdth
7.5      % Height of foam block (cm)                                  blockHght
10       % How far the foam block is from the prime cut head (cm)     blckOfstS
2.0      % How far the foam block is along cutting bed (cm)           blckOfstW
-1.0      % How far the foam block above the cutting bed (cm)          blckOfstH

% Input files for aerofoils: 'root.aero' and 'tip.aero' in xfoil format

% Alex Alderslade, Arman Salimian, Ross Calvert, Andrea Casanueva)";

	if (gcodePath.back() != '\\')
		gcodePath += '\\';

	std::ofstream settingsFile(gcodePath + "settings.txt");
	settingsFile << sweepDataLine << taperRatioDataLine << twistDataLine << rootChordDataLine << dihedralDataLine << settingsData;

	std::ofstream rootFile(gcodePath + "root.aero");
	const auto& rootCoords = rootGeom.coords;
	for (const auto& coord : rootCoords)
		rootFile << coord.x << ' ' << coord.y << '\n';

	std::ofstream tipFile(gcodePath + "tip.aero");
	const auto& tipCoords = tipGeom.coords;
	for (const auto& coord : tipCoords)
		tipFile << coord.x << ' ' << coord.y << '\n';
}

void Wing::CalculatePerformance(AirfoilData& root, AirfoilData& tip, AirfoilGeom& rootGeom_, AirfoilGeom& tipGeom_, AirfoilMeta& tipMeta_, AirfoilMeta& rootMeta_)
{
	rootData = root;
	tipData = tip;
	rootGeom = rootGeom_;
	tipGeom = tipGeom_;
	rootMeta = rootMeta_;
	tipMeta = tipMeta_;

    // TODO unshittify 
    alpha0 = (root.alpha0 + tip.alpha0 - twist) / 2.0;    // alpha02d == alpha03d
	aspectRatio = (4 * halfSpan * halfSpan) / (rootChord * rootChord * (1 + taperRatio));
    
	double clSlope2d = 0.5 * (root.clSlope + tip.clSlope);
    double kappa = clSlope2d * glm::one_over_two_pi<double>();
    double tanHalfChordSweep = tanf(sweep) + (2 * (taperRatio - 1) / (1 + taperRatio)) / aspectRatio; // based on lambda(n), where n is 0 for L.E. and 1 for T.E. 
    clSlope = (glm::two_pi<double>() * aspectRatio) / (2 + sqrtf((aspectRatio * aspectRatio) / (kappa * kappa) * (1 + tanHalfChordSweep * tanHalfChordSweep) + 4));

    // TODO temp
    if (clSlope < 0.001)
    {
        maxClCd = 0.0;

        s_pWingMgr->m_bestWings[bestConfigId] = *this;
        return;
    }

    // TODO what is "Although this is not a rigorous approach" supposed to mean?
    double clmax2d = root.clMax < tip.clMax ? root.clMax : tip.clMax;
    clmax = 0.9 * clmax2d * cos(atan(tanHalfChordSweep + (1 - taperRatio) / (aspectRatio * aspectRatio * taperRatio)));
    alphaStall = clmax / clSlope + alpha0;

    const u32 numPts = 50;
    const double inc = (alphaStall - alpha0) / numPts;
    for (u32 i = 0; i < numPts; ++i)
    {
        const double alpha = i * inc + alpha0;

        const double cl = clSlope * (alpha - alpha0);
		const double cd =  0.5 * 
						   (GetAirfoilDragCoeff(&root, alpha) + GetAirfoilDragCoeff(&tip, alpha)) +
						   ((cl * cl * glm::one_over_pi<double>()) / (0.7 * aspectRatio));
		
        const double clcd = cl / cd;
        if (clcd > maxClCd)
        {
            maxClCd = clcd;
        }
    }

    // TODO is clcd the only characteristic to rank by?
  //  if (maxClCd > s_pWingMgr->m_bestWings[bestConfigId].maxClCd)
  //  {
		//s_pWingMgr->m_bestWings[bestConfigId] = *this;
  //      /*if (ApplyTipLoading(0, 300) < 4)
  //          *m_bestConfig = *this;*/
  //  }

    GenerateGeometry();
}

void Wing::ApplyTipLoading(LoadingTest& test, Mesh* pOutMesh)
{
	if (loadingTest.numCuts == test.numCuts && loadingTest.loading == test.loading && loadingTest.bAlign == test.bAlign)
		return;

    static const double g = 9.80665;
    const double loading = test.loading * g / 1000.0;

    GenerateGeometry();

    // Generate slices
    if (m_slices.size() != test.numCuts)
    {
        m_slices.clear();
        m_slices.resize(test.numCuts);
        double inc = 1.0 / (test.numCuts + 1);
        for (u32 i = 0; i < test.numCuts; ++i)
        {
            float t = inc * (i + 1);
            double totalZ = 0.0; // Align slice to the XY plane at the avg z of slice coords 

            for (u32 airfoilCoord = 0; airfoilCoord < m_tip.points.size(); ++airfoilCoord)
            {
                m_slices[i].AddPoint(m_root.points[airfoilCoord] * t + m_tip.points[airfoilCoord] * (1.0f - t));
                m_slices[i].points.back().z = halfSpan - m_slices[i].points.back().z;
                totalZ += m_slices[i].points.back().z;
            }

            double avgZ = totalZ / m_tip.points.size();
            for (auto& slicePt : m_slices[i].points)
            {
                slicePt.z = avgZ;
            }

            m_slices[i].Analyze();
        }
    }

    double deflection = 0;

    // Deflect slices
    for (u32 i = 0; i < test.numCuts; ++i)
    {
        const double P = loading; // TODO shear 
        const double L = (1.0 / (test.numCuts + 1)) * (i + 1) * halfSpan;
        const double EI = abs(foam.youngMod * m_slices[i].GetIxx());

        const double endLoadDefl = (P * L * L * L) / (3 * EI) / 100; // 1/10 mm = 1/100 cm 
        const double endLoadRotation = (P * L * L) / (2 * EI) / 100;
        const double M = P * L; // End moment
        const double endMomentDefl = (M * L * L) / (2 * EI) / 100;
        const double endMomentRotation = (M * L) / EI / 100;
        const double rotationDefl = (endLoadRotation + endMomentRotation) * L;

        deflection += endLoadDefl + endMomentDefl + rotationDefl;

        double totalZ = 0.0;

        for (auto& slicePt : m_slices[i].points)
        {
            slicePt = glm::rotateX(slicePt, (float)(endLoadRotation, endMomentRotation));
            totalZ += slicePt.z;
        }

        if (test.bAlign)
            m_slices[i].AlignToXY(totalZ / m_tip.points.size());

        for (int j = i + 1; j < test.numCuts; ++j)
        {
            totalZ = 0.0;
            for (auto& slicePt : m_slices[j].points)
            {
                slicePt = glm::rotateX(slicePt, (float)(endLoadRotation, endMomentRotation));
                totalZ += slicePt.z;
            }

            if(test.bAlign)
                m_slices[j].AlignToXY(totalZ / m_tip.points.size());
        }

        totalZ = 0.0;
        for (auto& tipPt : m_tip.points)
        {
            tipPt = glm::rotateX(tipPt, (float)(endLoadRotation, endMomentRotation));
            totalZ += tipPt.z;
        }

        if (test.bAlign)
            m_tip.AlignToXY(totalZ / m_tip.points.size());
    }

    // Deflect tip
    const double P = loading;
    const double L = halfSpan;
    const double EI = abs(foam.youngMod * m_tip.GetIxx());

    const double endLoadDefl = (P * L * L * L) / (3 * EI) / 100; // 1/10 mm = 1/100 cm 
    const double endLoadRotation = (P * L * L) / (2 * EI) / 100;
    const double M = P * L; // End moment
    const double endMomentDefl = (M * L * L) / (2 * EI) / 100;
    const double endMomentRotation = (M * L) / EI / 100;
    const double rotationDefl = (endLoadRotation + endMomentRotation) * L;

    deflection += endLoadDefl + endMomentDefl + rotationDefl;

    for (auto& tipPt : m_tip.points)
    {
        tipPt = glm::rotateX(tipPt, (float)(endLoadRotation, endMomentRotation));
    }

    //deflection = m_tip.points.back().y;

    if(pOutMesh)
        GenerateMesh(pOutMesh);

	test.deflection = deflection;
	loadingTest = test;
}

void Wing::SetToBest()
{
	*this = s_pWingMgr->m_bestWings[id];
}

const Wing* Wing::GetBestConfig() const
{
    return this;
    //return &s_pWingMgr->m_bestWings[id];
}

u32 Wing::GetId() const
{
    return id;
}

u32 Wing::GetRootAirfoilId() const
{
    return rootAirfoil;
}

u32 Wing::GetTipAirfoilId() const
{
    return tipAirfoil;
}

void Wing::SetRootAirfoilId(u32 id)
{
    rootAirfoil = id;
}

void Wing::SetTipAirfoilId(u32 id)
{
    tipAirfoil = id;
}

void Wing::SetMaxClCd(float val)
{
	maxClCd = val;
}

void Wing::SetSweep(float val)
{
	sweep = val;
}

double Wing::GetLiftCoeff(float alpha) const
{
	assert(alpha <= alphaStall);
	return clSlope * (alpha - alpha0);
}

double Wing::GetDragCoeff(float alpha) const
{
	assert(alpha <= alphaStall);

	double cl = GetLiftCoeff(alpha);
	return 0.5 * (GetAirfoilDragCoeff(&rootData, alpha) + GetAirfoilDragCoeff(&tipData, alpha)) + (cl * cl * glm::one_over_pi<double>()) / (0.7 * aspectRatio);
}

float Wing::GetAlpha0() const
{
	return alpha0;
}

float Wing::GetAlphaStall() const
{
	return alphaStall;
}

float Wing::GetSweep() const
{
	return sweep;
}

float Wing::GetTaperRatio() const
{
	return taperRatio;
}

float Wing::GetTwist() const
{
	return twist;
}

float Wing::GetDihedral() const
{
	return dihedral;
}

float Wing::GetRootChord() const
{
	return rootChord;
}

float Wing::GetClmax() const
{
	return clmax;
}

float Wing::GetClslope() const
{
	return clSlope;
}

float Wing::GetMaxClCd() const
{
	return maxClCd;
}

float Wing::GetHalfVol() const
{
	return halfVolume;
}

float Wing::NeedReduceDepth() const
{
	return bNeedReduceDepth;
}

float Wing::NeedReduceHeight() const
{
	return bNeedReduceHeight;
}

const Ring & Wing::GetTip() const
{
	return m_tip;
}

const Ring & Wing::GetRoot() const
{
	return m_root;
}

void Wing::SetTaperRatio(float val)
{
	taperRatio = val;
}

void Wing::SetTwist(float val)
{
	twist = val;
}

void Wing::SetDihedral(float val)
{
	dihedral = val;
}

void Wing::SetRootChord(float val)
{
	rootChord = val;
}

Ring::Ring()
{
    Reset(0);
}

bool Ring::Analyze()
{
    // TODO temp
    if (!localPts.size())
        localPts = points;

    // Calculate area 
    for (u32 i = 0; i < localPts.size() - 1; ++i)
        area += (localPts[i].x * localPts[i + 1].y - localPts[i + 1].x * localPts[i].y);
    area /= 2.0;

    if (!area)
    {
        return false;
    }

    // Calculate second moments of area 
    for (u32 i = 0; i < localPts.size() - 1; ++i)
    {
        // Segment [(ax ay) (bx by)]
        auto ax = localPts[i].x, ay = localPts[i].y;
        auto bx = localPts[i + 1].x, by = localPts[i + 1].y;
        double elemTrianArea = ax * by - bx * ay;

        Ixx += ((ay * ay + ay * by + by * by) * elemTrianArea); 
        Iyy += ((ax * ax + ax * bx + bx * bx) * elemTrianArea);
        Ixy += ((ax * by + 2 * ax * ay + 2 * bx * by + bx * ay) * elemTrianArea);
    }

    Ixx /= 12.0;
    Iyy /= 12.0;
    Ixy /= 24.0;

    // TODO why does my calculation of moms of area give different results than the gcodegen matlab script thingy?
    /*Ixx = Ixy = Iyy = 0;
    for(u32 i = 1; i < localPts.size() / 3; i+=3)
    {
        auto xa = localPts[0].x;
        auto ya = localPts[0].y;
    
        auto xb = localPts[i - 1].x;
        auto yb = localPts[i - 1].y;
    
        auto xc = localPts[i].x;
        auto yc = localPts[i].y;

        auto areaInc = abs((xb*ya - xa*yb) + (xc*yb - xb*yc) + (xa*yc - xc*ya)) / 2;
        Ixx += ((yc - ya)*(yc-ya) + (yc - ya)*(yb - ya) + (yb - ya)*(yb-ya))*(2 * areaInc / 12);
        Iyy += ((xc - xa)*(xc-xa) + (xc - xa)*(xb - xa) + (xb - xa)*(xb-xa))*(2 * areaInc / 12);
        Ixy += ((xc - xa)*(yb - ya) + 2 * (xc - xa)*(yc - ya) + 2 * (xb - xa)*(yb - ya) + (xb - xa)*(yc - ya))*(2 * areaInc / 24);
    }*/

    // Calculate min thickness
    u32 numPerSide = localPts.size() / 2;
    for (u32 i = 1; i < numPerSide - 1; ++i)
    {
        // coords are not necessarily aligned on the y axis 
        for (u32 j = numPerSide + 1; j < localPts.size() - 1; ++j)
        {
            auto thickness = glm::length2(localPts[j] - localPts[i]);
            if (thickness < minThickness)
                minThickness = thickness;
        }
    }

    minThickness = sqrt(minThickness); // Length squared was used for comparison

    return true;
}

void Ring::Reset(u32 numPoints)
{
    points.clear();
    points.reserve(numPoints);
    Ixx = Iyy = Ixy = 0.0;
    area = 0.0;
    minThickness = std::numeric_limits<double>::max();
}

void Ring::AddPoint(float x, float y, float z)
{
    points.push_back({ x, y, z });
}

void Ring::AddPoint(const glm::vec3& newPt)
{
    points.push_back(newPt);
}

double Ring::GetArea() const
{
    return area;
}

double Ring::GetMinThickness() const
{
    return minThickness;
}

double Ring::GetIxx() const
{
    return Ixx;
}

double Ring::GetIyy() const
{
    return Iyy;
}

double Ring::GetIxy() const
{
    return Ixy;
}

void Ring::AlignToXY(float z)
{
    for (auto& pt : points)
        pt.z = z;
}

bool LoadingTest::operator!=(const LoadingTest & other) const
{
	return loading != other.loading || deflection != other.deflection || numCuts != other.numCuts || bAlign != other.bAlign;
}
