#pragma once
#include <thread>
#include <vector>
#include "common.h"


class WorkMgr
{
public:
    WorkMgr();
    ~WorkMgr();

private:
    u32 numHwThreads;
    std::vector<std::thread> threadPool;
};