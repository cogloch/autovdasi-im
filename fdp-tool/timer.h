#pragma once
#include <chrono>


class Timer
{
public:
	Timer();
	void Reset();
	double Tick(); // Returns the time elapsed since the last Tick() call, in millisec

private:
	std::chrono::time_point<std::chrono::steady_clock> m_prevTime;
};