#include "renderer.h"
#include "shaders.h"
#include "input.h"

#undef APIENTRY
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#include <glfw/glfw3native.h>


struct GLState
{
    void Store()
    {
        gl::GetIntegerv(gl::CURRENT_PROGRAM, &program);
        gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &texture);
        gl::GetIntegerv(gl::ACTIVE_TEXTURE, &activeTexture);
        gl::GetIntegerv(gl::ARRAY_BUFFER_BINDING, &arrayBuffer);
        gl::GetIntegerv(gl::ELEMENT_ARRAY_BUFFER_BINDING, &elementArrayBuffer);
        gl::GetIntegerv(gl::VERTEX_ARRAY_BINDING, &vertexArray);
        gl::GetIntegerv(gl::BLEND_SRC, &blendSrc);
        gl::GetIntegerv(gl::BLEND_DST, &blendDst);
        gl::GetIntegerv(gl::BLEND_EQUATION_RGB, &blendEquationRGB);
        gl::GetIntegerv(gl::BLEND_EQUATION_ALPHA, &blendEquationAlpha);
        gl::GetIntegerv(gl::VIEWPORT, viewport);
        gl::GetIntegerv(gl::SCISSOR_BOX, scissorBox);
        bEnableBlend = gl::IsEnabled(gl::BLEND);
        bEnableCullFace = gl::IsEnabled(gl::CULL_FACE);
        bEnableDepthTest = gl::IsEnabled(gl::DEPTH_TEST);
        bEnableScissorTest = gl::IsEnabled(gl::SCISSOR_TEST);
    }

    void Restore()
    {
        gl::UseProgram(program);
        gl::ActiveTexture(activeTexture);
        gl::BindTexture(gl::TEXTURE_2D, texture);
        gl::BindVertexArray(vertexArray);
        gl::BindBuffer(gl::ARRAY_BUFFER, arrayBuffer);
        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, elementArrayBuffer);
        gl::BlendEquationSeparate(blendEquationRGB, blendEquationAlpha);
        gl::BlendFunc(blendSrc, blendDst);
        if (bEnableBlend) gl::Enable(gl::BLEND); else gl::Disable(gl::BLEND);
        if (bEnableCullFace) gl::Enable(gl::CULL_FACE); else gl::Disable(gl::CULL_FACE);
        if (bEnableDepthTest) gl::Enable(gl::DEPTH_TEST); else gl::Disable(gl::DEPTH_TEST);
        if (bEnableScissorTest) gl::Enable(gl::SCISSOR_TEST); else gl::Disable(gl::SCISSOR_TEST);
        gl::Viewport(viewport[0], viewport[1], (GLsizei)viewport[2], (GLsizei)viewport[3]);
        gl::Scissor(scissorBox[0], scissorBox[1], (GLsizei)scissorBox[2], (GLsizei)scissorBox[3]);
    }

private:
    GLint program;
    GLint texture;
    GLint activeTexture; 
    GLint arrayBuffer; 
    GLint elementArrayBuffer; 
    GLint vertexArray; 
    GLint blendSrc; 
    GLint blendDst; 
    GLint blendEquationRGB; 
    GLint blendEquationAlpha; 
    GLint viewport[4]; 
    GLint scissorBox[4]; 
    GLboolean bEnableBlend;
    GLboolean bEnableCullFace;
    GLboolean bEnableDepthTest;
    GLboolean bEnableScissorTest;
};

void Renderer::RenderDrawLists(ImDrawData* draw_data)
{
    // Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
    auto& io = ImGui::GetIO();
    int fb_width = (int)(io.DisplaySize.x * io.DisplayFramebufferScale.x);
    int fb_height = (int)(io.DisplaySize.y * io.DisplayFramebufferScale.y);
    if (fb_width == 0 || fb_height == 0)
        return;
    draw_data->ScaleClipRects(io.DisplayFramebufferScale);

    // Backup GL state
    GLState prevState;
    prevState.Store();
    
    // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
    gl::Enable(gl::BLEND);
    //gl::BlendEquation(gl::FUNC_ADD);
    gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
    gl::Disable(gl::CULL_FACE);
    gl::Disable(gl::DEPTH_TEST);
    gl::Enable(gl::SCISSOR_TEST);
    gl::ActiveTexture(gl::TEXTURE0);

    // Setup viewport, orthographic projection matrix
    gl::Viewport(0, 0, (GLsizei)fb_width, (GLsizei)fb_height);
    const float ortho_projection[4][4] =
    {
        { 2.0f / io.DisplaySize.x, 0.0f,                   0.0f, 0.0f },
        { 0.0f,                  2.0f / -io.DisplaySize.y, 0.0f, 0.0f },
        { 0.0f,                  0.0f,                  -1.0f, 0.0f },
        { -1.0f,                  1.0f,                   0.0f, 1.0f },
    };
    gl::UseProgram(shaderHandle);
    gl::Uniform1i(attribLocTex, 0);
    gl::UniformMatrix4fv(attribLocProj, 1, gl::FALSE_, &ortho_projection[0][0]);
    gl::BindVertexArray(vaoHandle);

    for (int n = 0; n < draw_data->CmdListsCount; n++)
    {
        const ImDrawList* cmd_list = draw_data->CmdLists[n];
        const ImDrawIdx* idx_buffer_offset = 0;

        gl::BindBuffer(gl::ARRAY_BUFFER, vboHandle);
        gl::BufferData(gl::ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.Size * sizeof(ImDrawVert), (const GLvoid*)cmd_list->VtxBuffer.Data, gl::STREAM_DRAW);

        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, eboHandle);
        gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx), (const GLvoid*)cmd_list->IdxBuffer.Data, gl::STREAM_DRAW);

        for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
        {
            const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
            if (pcmd->UserCallback)
            {
                pcmd->UserCallback(cmd_list, pcmd);
            }
            else
            {
                gl::BindTexture(gl::TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
                gl::Scissor((int)pcmd->ClipRect.x, (int)(fb_height - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
                gl::DrawElements(gl::TRIANGLES, (GLsizei)pcmd->ElemCount, gl::UNSIGNED_SHORT, idx_buffer_offset);
            }
            idx_buffer_offset += pcmd->ElemCount;
        }
    }

    prevState.Restore();
}

void Renderer::CreateFontsTexture()
{
    ImGuiIO& io = ImGui::GetIO();
    unsigned char* pixels;
    int width, height;
    io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

    GLint last_texture;
    gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &last_texture);
    gl::GenTextures(1, &fontTexture);
    gl::BindTexture(gl::TEXTURE_2D, fontTexture);
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR);
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR);
    gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA, width, height, 0, gl::RGBA, gl::UNSIGNED_BYTE, pixels);

    io.Fonts->TexID = (void *)(intptr_t)fontTexture;

    gl::BindTexture(gl::TEXTURE_2D, last_texture);
}

void Renderer::CreateDeviceObjects()
{
    // Backup GL state
    GLint last_texture, last_array_buffer, last_vertex_array;
    gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &last_texture);
    gl::GetIntegerv(gl::ARRAY_BUFFER_BINDING, &last_array_buffer);
    gl::GetIntegerv(gl::VERTEX_ARRAY_BINDING, &last_vertex_array);

    const std::string vertShaderSrc = R"(
                #version 330 core
                uniform mat4 projection;
                in vec2 pos;
                in vec2 uv;
                in vec4 color;
                out vec2 fragUV;
                out vec4 fragColor; 
                void main()
                {
                    fragUV = uv;
                    fragColor = color;
                    gl_Position = projection * vec4(pos.xy, 0, 1);
                })";

    const std::string fragShaderSrc = R"(
                #version 330 core
                uniform sampler2D Texture;
                in vec2 fragUV;
                in vec4 fragColor;
                out vec4 outColor;
                void main()
                {
                    outColor = fragColor * texture(Texture, fragUV.st);
                })";

    shaderHandle = shaders::CreateProgram(vertShaderSrc, fragShaderSrc);

    attribLocTex = gl::GetUniformLocation(shaderHandle, "Texture");
    attribLocProj = gl::GetUniformLocation(shaderHandle, "projection");
    attribLocPos = gl::GetAttribLocation(shaderHandle, "pos");
    attribLocUV = gl::GetAttribLocation(shaderHandle, "uv");
    attribLocColor = gl::GetAttribLocation(shaderHandle, "color");

    gl::GenBuffers(1, &vboHandle);
    gl::GenBuffers(1, &eboHandle);

    gl::GenVertexArrays(1, &vaoHandle);
    gl::BindVertexArray(vaoHandle);
    gl::BindBuffer(gl::ARRAY_BUFFER, vboHandle);
    gl::EnableVertexAttribArray(attribLocPos);
    gl::EnableVertexAttribArray(attribLocUV);
    gl::EnableVertexAttribArray(attribLocColor);

#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))
    gl::VertexAttribPointer(attribLocPos, 2, gl::FLOAT, gl::FALSE_, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, pos));
    gl::VertexAttribPointer(attribLocUV, 2, gl::FLOAT, gl::FALSE_, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, uv));
    gl::VertexAttribPointer(attribLocColor, 4, gl::UNSIGNED_BYTE, gl::TRUE_, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, col));
#undef OFFSETOF

    CreateFontsTexture();

    // Restore modified GL state
    gl::BindTexture(gl::TEXTURE_2D, last_texture);
    gl::BindBuffer(gl::ARRAY_BUFFER, last_array_buffer);
    gl::BindVertexArray(last_vertex_array);

    bResourcesLoaded = true;
}

Renderer::Renderer(GLFWwindow* pWindow)
{
    m_context.Init(glfwGetWin32Window(pWindow), 1024, 768, 3, 3);
    Mesh::InitCommonResources();

    ImVec4 clearColor = ImColor(234, 160, 234);
    gl::ClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w);

}

void Renderer::NewFrame()
{
    if (!bResourcesLoaded)
        CreateDeviceObjects();
}

void Renderer::DrawFrame(const Camera& camera)
{
    wingMesh.Render(camera, light);
    ImGui::Render();
    auto uiDrawData = ImGui::GetDrawData();
    if(uiDrawData)
        RenderDrawLists(uiDrawData);
}

Mesh& Renderer::GetWingMeshRef()
{
    return wingMesh;
}
