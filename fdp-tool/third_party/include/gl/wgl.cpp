#include "wgl.hpp"
#include <vector>
#include <algorithm>
#include <string>


#ifdef _MSC_VER
#pragma warning(disable: 4055)
#pragma warning(disable: 4054)
#pragma warning(disable: 4996)
#endif

static int TestPointer(const PROC pTest)
{
	ptrdiff_t iTest;
	if(!pTest) return 0;
	iTest = (ptrdiff_t)pTest;
	
	if(iTest == 1 || iTest == 2 || iTest == 3 || iTest == -1) return 0;
	
	return 1;
}

static PROC WinGetProcAddress(const char *name)
{
#pragma warning(push)
#pragma warning(disable:6387)
	PROC pFunc = wglGetProcAddress((LPCSTR)name);
	if(TestPointer(pFunc))
	{
		return pFunc;
	}
	auto glMod = GetModuleHandleA("OpenGL32.dll");
	return (PROC)GetProcAddress(glMod, (LPCSTR)name);
#pragma warning(pop)
}
	
#define IntGetProcAddress(name) WinGetProcAddress(name)

namespace wgl
{
	namespace exts
	{
		LoadTest var_ARB_multisample;
		LoadTest var_ARB_extensions_string;
		LoadTest var_ARB_pixel_format;
		LoadTest var_ARB_pixel_format_float;
		LoadTest var_ARB_framebuffer_sRGB;
		LoadTest var_ARB_create_context;
		LoadTest var_ARB_create_context_profile;
		LoadTest var_ARB_create_context_robustness;
		LoadTest var_EXT_swap_control;
		LoadTest var_EXT_pixel_format_packed_float;
		LoadTest var_EXT_create_context_es2_profile;
		LoadTest var_EXT_swap_control_tear;
		LoadTest var_NV_swap_group;
		
	} //namespace exts
	
	namespace _detail
	{
		typedef const char * (CODEGEN_FUNCPTR *PFNGETEXTENSIONSSTRINGARB)(HDC);
		PFNGETEXTENSIONSSTRINGARB GetExtensionsStringARB = nullptr;
		
		static int Load_ARB_extensions_string()
		{
			int numFailed = 0;
			GetExtensionsStringARB = reinterpret_cast<PFNGETEXTENSIONSSTRINGARB>(IntGetProcAddress("wglGetExtensionsStringARB"));
			if(!GetExtensionsStringARB) ++numFailed;
			return numFailed;
		}
		
		typedef BOOL (CODEGEN_FUNCPTR *PFNCHOOSEPIXELFORMATARB)(HDC, const int *, const FLOAT *, UINT, int *, UINT *);
		PFNCHOOSEPIXELFORMATARB ChoosePixelFormatARB = nullptr;
		typedef BOOL (CODEGEN_FUNCPTR *PFNGETPIXELFORMATATTRIBFVARB)(HDC, int, int, UINT, const int *, FLOAT *);
		PFNGETPIXELFORMATATTRIBFVARB GetPixelFormatAttribfvARB = nullptr;
		typedef BOOL (CODEGEN_FUNCPTR *PFNGETPIXELFORMATATTRIBIVARB)(HDC, int, int, UINT, const int *, int *);
		PFNGETPIXELFORMATATTRIBIVARB GetPixelFormatAttribivARB = nullptr;
		
		static int Load_ARB_pixel_format()
		{
			int numFailed = 0;
			ChoosePixelFormatARB = reinterpret_cast<PFNCHOOSEPIXELFORMATARB>(IntGetProcAddress("wglChoosePixelFormatARB"));
			if(!ChoosePixelFormatARB) ++numFailed;
			GetPixelFormatAttribfvARB = reinterpret_cast<PFNGETPIXELFORMATATTRIBFVARB>(IntGetProcAddress("wglGetPixelFormatAttribfvARB"));
			if(!GetPixelFormatAttribfvARB) ++numFailed;
			GetPixelFormatAttribivARB = reinterpret_cast<PFNGETPIXELFORMATATTRIBIVARB>(IntGetProcAddress("wglGetPixelFormatAttribivARB"));
			if(!GetPixelFormatAttribivARB) ++numFailed;
			return numFailed;
		}
		
		typedef HGLRC (CODEGEN_FUNCPTR *PFNCREATECONTEXTATTRIBSARB)(HDC, HGLRC, const int *);
		PFNCREATECONTEXTATTRIBSARB CreateContextAttribsARB = nullptr;
		
		static int Load_ARB_create_context()
		{
			int numFailed = 0;
			CreateContextAttribsARB = reinterpret_cast<PFNCREATECONTEXTATTRIBSARB>(IntGetProcAddress("wglCreateContextAttribsARB"));
			if(!CreateContextAttribsARB) ++numFailed;
			return numFailed;
		}
		
		typedef int (CODEGEN_FUNCPTR *PFNGETSWAPINTERVALEXT)();
		PFNGETSWAPINTERVALEXT GetSwapIntervalEXT = nullptr;
		typedef BOOL (CODEGEN_FUNCPTR *PFNSWAPINTERVALEXT)(int);
		PFNSWAPINTERVALEXT SwapIntervalEXT = nullptr;
		
		static int Load_EXT_swap_control()
		{
			int numFailed = 0;
			GetSwapIntervalEXT = reinterpret_cast<PFNGETSWAPINTERVALEXT>(IntGetProcAddress("wglGetSwapIntervalEXT"));
			if(!GetSwapIntervalEXT) ++numFailed;
			SwapIntervalEXT = reinterpret_cast<PFNSWAPINTERVALEXT>(IntGetProcAddress("wglSwapIntervalEXT"));
			if(!SwapIntervalEXT) ++numFailed;
			return numFailed;
		}
		
		typedef BOOL (CODEGEN_FUNCPTR *PFNBINDSWAPBARRIERNV)(GLuint, GLuint);
		PFNBINDSWAPBARRIERNV BindSwapBarrierNV = nullptr;
		typedef BOOL (CODEGEN_FUNCPTR *PFNJOINSWAPGROUPNV)(HDC, GLuint);
		PFNJOINSWAPGROUPNV JoinSwapGroupNV = nullptr;
		typedef BOOL (CODEGEN_FUNCPTR *PFNQUERYFRAMECOUNTNV)(HDC, GLuint *);
		PFNQUERYFRAMECOUNTNV QueryFrameCountNV = nullptr;
		typedef BOOL (CODEGEN_FUNCPTR *PFNQUERYMAXSWAPGROUPSNV)(HDC, GLuint *, GLuint *);
		PFNQUERYMAXSWAPGROUPSNV QueryMaxSwapGroupsNV = nullptr;
		typedef BOOL (CODEGEN_FUNCPTR *PFNQUERYSWAPGROUPNV)(HDC, GLuint *, GLuint *);
		PFNQUERYSWAPGROUPNV QuerySwapGroupNV = nullptr;
		typedef BOOL (CODEGEN_FUNCPTR *PFNRESETFRAMECOUNTNV)(HDC);
		PFNRESETFRAMECOUNTNV ResetFrameCountNV = nullptr;
		
		static int Load_NV_swap_group()
		{
			int numFailed = 0;
			BindSwapBarrierNV = reinterpret_cast<PFNBINDSWAPBARRIERNV>(IntGetProcAddress("wglBindSwapBarrierNV"));
			if(!BindSwapBarrierNV) ++numFailed;
			JoinSwapGroupNV = reinterpret_cast<PFNJOINSWAPGROUPNV>(IntGetProcAddress("wglJoinSwapGroupNV"));
			if(!JoinSwapGroupNV) ++numFailed;
			QueryFrameCountNV = reinterpret_cast<PFNQUERYFRAMECOUNTNV>(IntGetProcAddress("wglQueryFrameCountNV"));
			if(!QueryFrameCountNV) ++numFailed;
			QueryMaxSwapGroupsNV = reinterpret_cast<PFNQUERYMAXSWAPGROUPSNV>(IntGetProcAddress("wglQueryMaxSwapGroupsNV"));
			if(!QueryMaxSwapGroupsNV) ++numFailed;
			QuerySwapGroupNV = reinterpret_cast<PFNQUERYSWAPGROUPNV>(IntGetProcAddress("wglQuerySwapGroupNV"));
			if(!QuerySwapGroupNV) ++numFailed;
			ResetFrameCountNV = reinterpret_cast<PFNRESETFRAMECOUNTNV>(IntGetProcAddress("wglResetFrameCountNV"));
			if(!ResetFrameCountNV) ++numFailed;
			return numFailed;
		}
		
	} //namespace _detail
	
	namespace sys
	{
		namespace 
		{
			typedef int (*PFN_LOADEXTENSION)();
			struct MapEntry
			{
				MapEntry(const std::string& extName_, exts::LoadTest* extVariable_, PFN_LOADEXTENSION loaderFunc_ = nullptr)
					: extName(extName_)
					, extVariable(extVariable_)
					, loaderFunc(loaderFunc_)
				{
				}
				
				std::string extName;
				exts::LoadTest *extVariable;
				PFN_LOADEXTENSION loaderFunc;
			};
			
			struct MapCompare
			{
				MapCompare(const std::string& test_) 
			        : test(test_)
				{
				}

				bool operator()(const MapEntry &other) const 
			    { 
                    return test == other.extName; 
			    }

				std::string test;
			};
			
			void InitializeMappingTable(std::vector<MapEntry>& table)
			{
				table.reserve(13);
				table.push_back(MapEntry("WGL_ARB_multisample", &exts::var_ARB_multisample));
				table.push_back(MapEntry("WGL_ARB_extensions_string", &exts::var_ARB_extensions_string, _detail::Load_ARB_extensions_string));
				table.push_back(MapEntry("WGL_ARB_pixel_format", &exts::var_ARB_pixel_format, _detail::Load_ARB_pixel_format));
				table.push_back(MapEntry("WGL_ARB_pixel_format_float", &exts::var_ARB_pixel_format_float));
				table.push_back(MapEntry("WGL_ARB_framebuffer_sRGB", &exts::var_ARB_framebuffer_sRGB));
				table.push_back(MapEntry("WGL_ARB_create_context", &exts::var_ARB_create_context, _detail::Load_ARB_create_context));
				table.push_back(MapEntry("WGL_ARB_create_context_profile", &exts::var_ARB_create_context_profile));
				table.push_back(MapEntry("WGL_ARB_create_context_robustness", &exts::var_ARB_create_context_robustness));
				table.push_back(MapEntry("WGL_EXT_swap_control", &exts::var_EXT_swap_control, _detail::Load_EXT_swap_control));
				table.push_back(MapEntry("WGL_EXT_pixel_format_packed_float", &exts::var_EXT_pixel_format_packed_float));
				table.push_back(MapEntry("WGL_EXT_create_context_es2_profile", &exts::var_EXT_create_context_es2_profile));
				table.push_back(MapEntry("WGL_EXT_swap_control_tear", &exts::var_EXT_swap_control_tear));
				table.push_back(MapEntry("WGL_NV_swap_group", &exts::var_NV_swap_group, _detail::Load_NV_swap_group));
			}
			
			void ClearExtensionVars()
			{
				exts::var_ARB_multisample = exts::LoadTest();
				exts::var_ARB_extensions_string = exts::LoadTest();
				exts::var_ARB_pixel_format = exts::LoadTest();
				exts::var_ARB_pixel_format_float = exts::LoadTest();
				exts::var_ARB_framebuffer_sRGB = exts::LoadTest();
				exts::var_ARB_create_context = exts::LoadTest();
				exts::var_ARB_create_context_profile = exts::LoadTest();
				exts::var_ARB_create_context_robustness = exts::LoadTest();
				exts::var_EXT_swap_control = exts::LoadTest();
				exts::var_EXT_pixel_format_packed_float = exts::LoadTest();
				exts::var_EXT_create_context_es2_profile = exts::LoadTest();
				exts::var_EXT_swap_control_tear = exts::LoadTest();
				exts::var_NV_swap_group = exts::LoadTest();
			}
			
			void LoadExtByName(std::vector<MapEntry> &table, const char *extensionName)
			{
				auto entry = std::find_if(table.begin(), table.end(), MapCompare(extensionName));
				
				if(entry != table.end())
				{
					if(entry->loaderFunc)
						(*entry->extVariable) = exts::LoadTest(true, entry->loaderFunc());
					else
						(*entry->extVariable) = exts::LoadTest(true, 0);
				}
			}
		} //namespace 
		
		
		namespace 
		{
			void ProcExtsFromExtString(const char *strExtList, std::vector<MapEntry> &table)
			{
				size_t iExtListLen = strlen(strExtList);
				const char *strExtListEnd = strExtList + iExtListLen;
				const char *strCurrPos = strExtList;
				char strWorkBuff[256];
			
				while(*strCurrPos)
				{
					// Get the extension at our position.
					const char *strEndStr = strchr(strCurrPos, ' ');
					int iStop = 0;
					if(!strEndStr)
					{
						strEndStr = strExtListEnd;
						iStop = 1;
					}
			
					auto iStrLen = (int)((ptrdiff_t)strEndStr - (ptrdiff_t)strCurrPos);
			
					if(iStrLen > 255)
						return;
			
					strncpy(strWorkBuff, strCurrPos, iStrLen);
					strWorkBuff[iStrLen] = '\0';
			
					LoadExtByName(table, strWorkBuff);
			
					strCurrPos = strEndStr + 1;
					if(iStop) break;
				}
			}
			
		} //namespace 
		
		exts::LoadTest LoadFunctions(HDC hdc)
		{
			ClearExtensionVars();
			std::vector<MapEntry> table;
			InitializeMappingTable(table);
			
			_detail::GetExtensionsStringARB = reinterpret_cast<_detail::PFNGETEXTENSIONSSTRINGARB>(IntGetProcAddress("wglGetExtensionsStringARB"));
			if(!_detail::GetExtensionsStringARB) return exts::LoadTest();
			
			ProcExtsFromExtString((const char *)wgl::_detail::GetExtensionsStringARB(hdc), table);
			return exts::LoadTest(true, 0);
		}
		
	} //namespace sys
} //namespace wgl