#pragma once
#include <gl/glcore.hpp>
#include <glfw/glfw3.h>
#include <imgui/imgui.h>
#include "mesh.h"
#include "light.h"
#include "glcontext.h"


class Renderer
{
public:
    Renderer(GLFWwindow* pWindow);
    void NewFrame();
    void DrawFrame(const Camera&);

    Mesh& GetWingMeshRef();

    PointLight light = PointLight({ 0.0f, -32.f, 0.f }, { 1.f, 1.f, 1.f });

private:
    Mesh wingMesh;

    void RenderDrawLists(ImDrawData*);
    void CreateFontsTexture();
    void CreateDeviceObjects();

    // UI
    bool bResourcesLoaded = false;
    int shaderHandle = 0;
    unsigned int vboHandle = 0, vaoHandle = 0, eboHandle = 0;
    int attribLocTex = 0, attribLocProj = 0, attribLocPos = 0, attribLocUV = 0, attribLocColor = 0;
    GLuint fontTexture = 0;

    GLContext m_context;
};