﻿#include "ui.h"
#include <imgui\imgui.h>
#include "wing_mgr.h"


UIState::UIState()
	: config(0, 0, 0)
{
	ResetOneoff();

	airfoilViz.parentWindow = "airfoilviz";
	airfoilViz.bAllowStretching = false;
	clvsalpha.parentWindow = "clvsalpha";
	cdvsalpha.parentWindow = "cdvsalpha";
	clcd3d.parentWindow = "clcd3d";
}

void UIState::NewFrame(float dt)
{
	ResetOneoff();

	auto& io = ImGui::GetIO();
	io.DeltaTime = dt;
	ImGui::NewFrame();

	ShowAirfoilBrowser();
	ShowDataViz();
	ShowConfigBrowser();
}

void UIState::ShowAirfoilBrowser()
{
	const auto numAirfoils = pAirfoilMgr->GetNumAirfoils();

	ImGui::Begin("airfoil db", nullptr, ImGuiWindowFlags_NoTitleBar);
    ImGui::Text(u8"Airfoil entries in database: %d (generated at max 10 iter/alpha, 26 alphas between -5..+20, Re# = 595593)", numAirfoils);
			
	ImGui::BeginChild("##scrollingregion", ImVec2(0, 0));
    ImGui::Columns(6);
    ImGui::Separator();
    ImGui::Text("Code"); ImGui::NextColumn();
    ImGui::Text("Takeoff"); ImGui::NextColumn();
    ImGui::Text("Dash"); ImGui::NextColumn();
    ImGui::Text("Loiter"); ImGui::NextColumn();
    ImGui::Text("Land"); ImGui::NextColumn();
    ImGui::Text("Total"); ImGui::NextColumn();
    ImGui::Separator();

	ImGui::Columns(6);
	for (u32 i = 0; i < numAirfoils; ++i)
	{
		const auto& meta = pAirfoilMgr->GetAirfoilMeta(i);
        const auto& score = pAirfoilMgr->GetAirfoilConstRef(i).scorecard;
        const auto& perf = pAirfoilMgr->GetAirfoilConstRef(i);

		if (ImGui::Selectable(std::string("NACA " + meta.name).c_str(), selectedAirfoil == i, ImGuiSelectableFlags_SpanAllColumns))
			selectedAirfoil = i;
		ImGui::NextColumn();
        ImGui::Text("%i(%.4lf)", score.takeoffRank, perf.takeoffPerf);
        ImGui::NextColumn();
        ImGui::Text("%i(%.4lf)", score.dashRank, perf.cd0);
        ImGui::NextColumn();
        ImGui::Text("%i(%.4lf)", score.loiterRank, perf.loiterCd);
        ImGui::NextColumn();
        ImGui::Text("%i(%.4lf)", score.landRank, perf.clMax);
        ImGui::NextColumn();
        ImGui::Text("%i(#%i)", score.totalRank, i + 1);
        ImGui::NextColumn();

		ImGui::Separator();
	}
	ImGui::Columns(6);
	ImGui::EndChild();

	ImGui::End();
}

void UIState::ShowDataViz()
{
	Wing oldConfig(0, 0, 0);

	if (selectedAirfoil != -1)
	{
		const auto& data = pAirfoilMgr->GetAirfoilConstRef(selectedAirfoil);
		const auto& geom = pAirfoilMgr->GetAirfoilConstGeom(selectedAirfoil);

        std::vector<glm::vec2> coords;
        for (u32 i = 0; i < geom.coords.size(); ++i)
        {
            coords.push_back({ geom.coords[i].x, -geom.coords[i].y });
        }

		airfoilViz.LinePlot(coords);

		std::vector<glm::vec2> clPts, cdPts;
		for (const auto& coef : data.coeffs)
		{
			clPts.push_back({ coef.alpha, coef.cl });
			cdPts.push_back({ coef.alpha, coef.cd });
		}
		clvsalpha.LinePlot(clPts);
		cdvsalpha.LinePlot(cdPts);
	}

	if (selectedWing == -1)
		return;

	ShowWingConfig();

	const auto& curWing = pWingMgr->GetWingConstRef(selectedWing);
	if (!curWing.IsValid())
		return;

	if (curWing != oldConfig)
		bWingConfigChanged = true;

	// CL/CD
	const u32 numPts = 20;
	std::vector<glm::vec2> pts;
	const double inc = (curWing.GetAlphaStall() - curWing.GetAlpha0()) / numPts;
	for (u32 i = 0; i < numPts; ++i)
	{
		const double alpha = i * inc + curWing.GetAlpha0();
		const double cd = curWing.GetDragCoeff(alpha);
		const double cl = curWing.GetLiftCoeff(alpha);
		pts.push_back({ cd, cl });
	}

	clcd3d.LinePlot(pts);

	oldConfig = curWing;
}

void UIState::ShowWingConfig()
{
	config = pWingMgr->GetWingConstRef(selectedWing);
	Wing configBackup = config;
	const auto numAirfoils = pAirfoilMgr->GetNumAirfoils();

	ImGui::Begin("wingconifg", nullptr, ImGuiWindowFlags_NoTitleBar);
	u32 rootAirfoilId = config.GetRootAirfoilId();
	if (INVALID_AIRFOIL_ID == rootAirfoilId)
	{
		ImGui::Text("Root airfoil: None");
	}
	else
	{
		const auto& rootAirfoil = pAirfoilMgr->GetAirfoilMeta(rootAirfoilId);

		if (ImGui::Button(("Root airfoil: " + rootAirfoil.name).c_str()))
		{
			for (u32 i = 0; i < numAirfoils; ++i)
				if (pAirfoilMgr->GetAirfoilMeta(i).name == rootAirfoil.name)
				{
					selectedAirfoil = i;
					break;
				}
		}
	}

	ImGui::SameLine();
	if (ImGui::Button("Use selected for root") && selectedAirfoil != -1)
		config.SetRootAirfoilId(selectedAirfoil);

	u32 tipAirfoilId = config.GetTipAirfoilId();
	if (INVALID_AIRFOIL_ID == tipAirfoilId)
	{
		ImGui::Text("Tip airfoil: None");
	}
	else
	{
		const auto& tipAirfoil = pAirfoilMgr->GetAirfoilMeta(tipAirfoilId);

		if (ImGui::Button(("Tip airfoil: " + tipAirfoil.name).c_str()))
		{
			for (u32 i = 0; i < numAirfoils; ++i)
				if (pAirfoilMgr->GetAirfoilMeta(i).name == tipAirfoil.name)
				{
					selectedAirfoil = i;
					break;
				}
		}
	}

	ImGui::SameLine();
	if (ImGui::Button("Use selected for tip") && selectedAirfoil != -1)
		config.SetTipAirfoilId(selectedAirfoil);

	rootAirfoilId = config.GetRootAirfoilId();
	tipAirfoilId = config.GetTipAirfoilId();

	const auto& rootAirfoil = pAirfoilMgr->GetAirfoilConstGeom(rootAirfoilId);
	const auto& tipAirfoil = pAirfoilMgr->GetAirfoilConstGeom(tipAirfoilId);

	if (rootAirfoilId != INVALID_AIRFOIL_ID && tipAirfoilId != INVALID_AIRFOIL_ID)
		if (tipAirfoil.coords.size() != rootAirfoil.coords.size())
			ImGui::TextColored({ 255, 0, 0, 255 }, "Dissimilar airfoils for tip&chord must have the same number of points.");

	float sweep = config.GetSweep();
	float taperRatio = config.GetTaperRatio();
	float twist = config.GetTwist();
	float dihedral = config.GetDihedral();
	float rootChord = config.GetRootChord();

	ImGui::SliderFloat("Sweep", &sweep, 0.f, 30.f);
	ImGui::SliderFloat("Taper ratio", &taperRatio, 0.5f, 1.f);
	ImGui::SliderFloat("Twist", &twist, 0.0f, 10.0f);
	ImGui::SliderFloat("Dihedral", &dihedral, 0.0f, 10.0f);
	ImGui::SliderFloat("Root chord", &rootChord, 10.0f, 30.0f);

	config.SetSweep(sweep);
	config.SetTaperRatio(taperRatio);
	config.SetTwist(twist);
	config.SetDihedral(dihedral);
	config.SetRootChord(rootChord);

    pWingMgr->GetWingRef(selectedWing) = config;
    needsUpdating.insert(selectedWing);

	ImGui::Separator();

	ImGui::Text("Max CL = %lf", config.GetClmax());
	ImGui::Text("Max CL incidence angle = %.2lf", config.GetAlphaStall());
	ImGui::Text("CL slope = %lf", config.GetClslope());
	ImGui::Text("Zero lift incidence = %.2lf", config.GetAlpha0());
	ImGui::Text("Max CL/CD = %lf", config.GetMaxClCd());

	if (configBackup != config)
		config.CalculatePerformance(config.rootData, config.tipData, config.rootGeom, config.tipGeom, config.tipMeta, config.rootMeta);

	ImGui::Text("Root area: %lf", config.GetRoot().GetArea());
	ImGui::SameLine();
	ImGui::Text("Tip area: %lf", config.GetTip().GetArea());
	ImGui::Text("Min thickness: %lf", config.GetTip().GetMinThickness());
	ImGui::SameLine();
	if (config.GetTip().GetMinThickness() < Ring::minThicknessThreshold)
		ImGui::TextColored(ImColor(255, 0, 0), "Too thin!");
	else
		ImGui::TextColored(ImColor(0, 255, 0), "Thick enough");
	if (config.NeedReduceDepth())
		ImGui::TextColored(ImColor(255, 0, 0), "Reduce depth(chordwise)!");
	if (config.NeedReduceHeight())
		ImGui::TextColored(ImColor(255, 0, 0), "Reduce height!");

	ImGui::Separator();

	ImGui::Text("Tip: Ixx = %lf, Iyy = %lf, Ixy = %lf", config.GetTip().GetIxx(), config.GetTip().GetIyy(), config.GetTip().GetIxy());
	ImGui::Text("Tip deflection: %.5lfcm", config.loadingTest.deflection);

	ImGui::Separator();

	ImGui::End();
}

void UIState::ShowConfigBrowser()
{
	ImGui::Begin("Config browser", nullptr, ImGuiWindowFlags_NoTitleBar);

	if (ImGui::Button("Create new config"))
	{
		//m_wingMgr.AddWing();
	}

	if (ImGui::Button("Save configs"))
		bSaveConfigs = true;

	if (ImGui::Button("(temp)Set to best config"))
		bSetToBestConfig = true;

	static bool bHideUseless = false;
	ImGui::Checkbox("Hide useless", &bHideUseless);

	static bool bHideStructuralFailures = false;
	ImGui::Checkbox("Hide structural failures", &bHideStructuralFailures);

	static u32 oldNumWings = 0;

	ImGui::SameLine();

	ImGui::Checkbox("Rank by current", &bRankByCurrent);

	static float maxDeflection = 0.f;
	static float oldMaxDeflection = 0.f;
	ImGui::SliderFloat("Max deflection", &maxDeflection, 0.f, 20.f);
	ImGui::SameLine();
	if (ImGui::SmallButton("Set test"))
		maxDeflection = 4.f;

	ImGui::BeginChild("Scrolling browser");
	ImGui::Columns(5);
	ImGui::Separator();
	ImGui::Text("Name"); ImGui::NextColumn();
	ImGui::Text("Validity"); ImGui::NextColumn();
	ImGui::Text("Max Cl/Cd"); ImGui::NextColumn();
	ImGui::Text("Best max Cl/Cd"); ImGui::NextColumn();
	ImGui::Text("Half volume"); ImGui::NextColumn();
	ImGui::Separator();

	for (u32 i = 0; i < pWingMgr->GetNumWings(); ++i)
	{
		auto& wing = pWingMgr->GetWingConstRef(i);
		auto pWing = pWingMgr->GetWingConstPtr(i);

		if (bHideUseless)
		{
			if (!wing.IsValid() || wing.NeedReduceDepth() || wing.NeedReduceHeight() || (wing.GetTip().GetMinThickness() < Ring::minThicknessThreshold))
				continue;
			if (!wing.GetHalfVol() || !wing.GetMaxClCd())
				continue;
		}

		if (bHideStructuralFailures)
		{
			if (!pWing) continue;

			if (wing.loadingTest.deflection > maxDeflection)
				continue;
		}

		if (ImGui::Selectable(std::string("Config #" + std::to_string(wing.GetId())).c_str(), selectedWing == i, ImGuiSelectableFlags_SpanAllColumns))
			selectedWing = i;
		ImGui::NextColumn();

		// TODO include volume limits into validitiy checking 
		if (wing.IsValid())
		{
			if (!wing.NeedReduceDepth() && !wing.NeedReduceHeight() && !(wing.GetTip().GetMinThickness() < Ring::minThicknessThreshold))
				ImGui::TextColored(ImColor(0, 255, 0), "OK");
			else
				ImGui::TextColored(ImColor(255, 0, 0), "NOT OK");
			ImGui::NextColumn();

			if (wing.GetMaxClCd() != std::numeric_limits<float>::lowest())
				ImGui::Text("%.4lf", wing.GetMaxClCd());
			ImGui::NextColumn();

			auto best = wing.GetBestConfig()->GetMaxClCd();
			if (best != std::numeric_limits<float>::lowest())
				ImGui::Text("%.4lf", best);
			ImGui::NextColumn();

			ImGui::Text("%.4lf", wing.GetHalfVol());
			ImGui::NextColumn();
		}
		else
		{
			ImGui::TextColored(ImColor(255, 0, 0), "NOT OK");
			ImGui::NextColumn();
			ImGui::NextColumn();
			ImGui::NextColumn();
			ImGui::NextColumn();
		}

		ImGui::Separator();
	}
	ImGui::Columns(1);
	ImGui::EndChild();

	ImGui::End();
}

void UIState::ResetOneoff()
{
	bWingConfigChanged = false;
    bSaveConfigs = false;
	bSetToBestConfig = false;
	needsUpdating.clear();
}

