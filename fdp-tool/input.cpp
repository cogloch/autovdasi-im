#include "input.h"
#include "imgui\imgui.h"


namespace input
{
    void Tick(GLFWwindow* pWindow)
    {
        auto& io = ImGui::GetIO();

        if (glfwGetWindowAttrib(pWindow, GLFW_FOCUSED))
        {
            double mouseX, mouseY;
            glfwGetCursorPos(pWindow, &mouseX, &mouseY);
            io.MousePos = { (float)mouseX, (float)mouseY };
        }
        else
        {
            io.MousePos = { -1, -1 };
        }

        for (int i = 0; i < 3; ++i)
        {
            io.MouseDown[i] = bMousePressed[i] || glfwGetMouseButton(pWindow, i) != 0;
            bMousePressed[i] = false;
        }

        io.MouseWheel = mouseWheel;
        mouseWheel = 0.0f;

        glfwSetInputMode(pWindow, GLFW_CURSOR, io.MouseDrawCursor ? GLFW_CURSOR_HIDDEN : GLFW_CURSOR_NORMAL);
    }

    bool bMousePressed[3] = { false, false, false };
    float mouseWheel = 0.0f;

	void SetCallbacks(GLFWwindow* pWindow)
	{
		// Mouse 
		glfwSetMouseButtonCallback(pWindow, [](GLFWwindow*, int button, int action, int) {
			if (action == GLFW_PRESS && button >= 0 && button < 3)
				bMousePressed[button] = true;
		});

		// Scroll
		glfwSetScrollCallback(pWindow, [](GLFWwindow*, double, double yoffset){
			mouseWheel += (float)yoffset;
		});

		// Key 
		glfwSetKeyCallback(pWindow, [](GLFWwindow*, int key, int, int action, int mods) {
			ImGuiIO& io = ImGui::GetIO();
			if (action == GLFW_PRESS)
				io.KeysDown[key] = true;
			if (action == GLFW_RELEASE)
				io.KeysDown[key] = false;

			(void)mods; // Modifiers are not reliable across systems
			io.KeyCtrl = io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
			io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
			io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
			io.KeySuper = io.KeysDown[GLFW_KEY_LEFT_SUPER] || io.KeysDown[GLFW_KEY_RIGHT_SUPER]; 
		});
		
		// Char
		glfwSetCharCallback(pWindow, [](GLFWwindow*, unsigned int c) {
			ImGuiIO& io = ImGui::GetIO();
			if (c > 0 && c < 0x10000)
				io.AddInputCharacter((unsigned short)c);
		});
	}
}