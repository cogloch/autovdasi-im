#include "mesh.h"
#include "shaders.h"
#include <imgui/imgui.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>


GLuint Mesh::shaderHandle = 0;
GLint Mesh::modelLoc = 0, Mesh::viewLoc = 0, Mesh::projLoc = 0;

Mesh::Vertex::Vertex(const glm::vec3& pos_) 
    : pos(pos_)
{
}

void Mesh::InitCommonResources()
{
    const std::string vertShaderSrc = R"(
                #version 330 core
                layout(location = 0)in vec3 pos;
                layout(location = 1)in vec3 normal;
                uniform mat4 model;
                uniform mat4 view;
                uniform mat4 projection;
                out vec3 fragNormal;
                out vec3 fragPos;
                void main()
                {
                    fragPos = vec3(model * vec4(pos, 1.0f));
                    fragNormal = mat3(transpose(inverse(model))) * normal;
                    gl_Position = projection * view * model * vec4(pos, 1.0f);
                })";

    const std::string fragShaderSrc = R"(
                #version 330 core
                out vec4 color;
                in vec3 fragPos;
                in vec3 fragNormal;
                uniform vec3 lightPos; 
                uniform vec3 viewPos;
                uniform vec3 lightColor;
                uniform vec3 objectColor;
                void main()
                {
                    // Ambient
                    float ambientStrength = 0.1f;
                    vec3 ambient = ambientStrength * lightColor;
  	
                    // Diffuse 
                    vec3 norm = normalize(fragNormal);
                    vec3 lightDir = normalize(lightPos - fragPos);
                    float diff = max(dot(norm, lightDir), 0.0);
                    vec3 diffuse = diff * lightColor;
    
                    // Specular
                    float specularStrength = 0.5f;
                    vec3 viewDir = normalize(viewPos - fragPos);
                    vec3 reflectDir = reflect(-lightDir, norm);  
                    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
                    vec3 specular = specularStrength * spec * lightColor;  
        
                    vec3 result = (ambient + diffuse + specular) * objectColor;
                    color = vec4(result, 1.0f);
                })";

    shaderHandle = shaders::CreateProgram(vertShaderSrc, fragShaderSrc);

    modelLoc = gl::GetUniformLocation(shaderHandle, "model");
    viewLoc = gl::GetUniformLocation(shaderHandle, "view");
    projLoc = gl::GetUniformLocation(shaderHandle, "projection");
}

// TODO stop leaking 
void Mesh::CreateGraphicsResources()
{
    gl::GenVertexArrays(1, &vao);
    gl::GenBuffers(1, &vbo);
    gl::GenBuffers(1, &ebo);

    gl::BindVertexArray(vao);

    gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
    gl::BufferData(gl::ARRAY_BUFFER, verts.size() * sizeof(Vertex), &verts[0], gl::STATIC_DRAW);

    gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ebo);
    gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], gl::STATIC_DRAW);

    // Position
    gl::EnableVertexAttribArray(0);
    gl::VertexAttribPointer(0, 3, gl::FLOAT, 0, sizeof(Vertex), (void*)(0));

    // Normal
    gl::EnableVertexAttribArray(1);
    gl::VertexAttribPointer(1, 3, gl::FLOAT, 0, sizeof(Vertex), (void*)offsetof(Vertex, normal));

    gl::BindVertexArray(0);

    bRenderable = true;
}

void Mesh::GenerateNormals()
{
    u32 numTriangles = indices.size() / 3;
    for (u32 i = 0; i < numTriangles; ++i)
    {
        u32 i0 = indices[i * 3 + 0],
            i1 = indices[i * 3 + 1],
            i2 = indices[i * 3 + 2];

        auto v0 = verts[i0].pos,
             v1 = verts[i1].pos,
             v2 = verts[i2].pos;

        auto faceNormal = glm::cross((v1 - v0), (v2 - v0));

        auto n0 = verts[i0].normal,
             n1 = verts[i1].normal,
             n2 = verts[i2].normal;

        verts[i0].normal = n0 + faceNormal;
        verts[i1].normal = n1 + faceNormal;
        verts[i2].normal = n2 + faceNormal;
    }

    for (auto& vert : verts)
        vert.normal = glm::normalize(vert.normal);
}

void Mesh::SetQuadFace(u32 v0, u32 v1, u32 v2, u32 v3)
{
    indices.push_back(v0);
    indices.push_back(v1);
    indices.push_back(v3);

    indices.push_back(v3);
    indices.push_back(v1);
    indices.push_back(v2);
}

void Mesh::Render(const Camera& camera, const PointLight& light)
{
    if (!bRenderable) return;

    gl::Enable(gl::DEPTH_TEST);

    gl::UseProgram(shaderHandle);

    glm::mat4 projection;
    ImGuiIO& io = ImGui::GetIO();
    int fb_width = (int)(io.DisplaySize.x * io.DisplayFramebufferScale.x);
    int fb_height = (int)(io.DisplaySize.y * io.DisplayFramebufferScale.y);
    if (fb_width == 0 || fb_height == 0)
        return;
    projection = glm::perspective(45.0f, (GLfloat)fb_width / (GLfloat)fb_height, 0.1f, 100.0f);
    // Pass the matrices to the shader
    gl::UniformMatrix4fv(viewLoc, 1, gl::FALSE_, glm::value_ptr(camera.GetViewMatrix()));
    // Note: currently we set the projection matrix each frame, but since the projection matrix rarely changes it's often best practice to set it outside the main loop only once.
    gl::UniformMatrix4fv(projLoc, 1, gl::FALSE_, glm::value_ptr(projection));

    gl::BindVertexArray(vao);

    // Lights
    gl::Uniform3fv(gl::GetUniformLocation(shaderHandle, "lightPos"), 1, glm::value_ptr(light.pos));
    gl::Uniform3fv(gl::GetUniformLocation(shaderHandle, "viewPos"), 1, glm::value_ptr(camera.m_position));
    gl::Uniform3fv(gl::GetUniformLocation(shaderHandle, "lightColor"), 1, glm::value_ptr(light.color));
    const glm::vec3 cornflowerBlue = { 100.f / 255.f, 149.f / 255.f, 237.f / 255.f }; // VERY IMPORTANT; Efficiency is priority number one, people, because waste is a thief  
    gl::Uniform3fv(gl::GetUniformLocation(shaderHandle, "objectColor"), 1, glm::value_ptr(cornflowerBlue));

    gl::UniformMatrix4fv(modelLoc, 1, gl::FALSE_, glm::value_ptr(xform));
    gl::DrawElements(gl::TRIANGLES, (GLsizei)indices.size(), gl::UNSIGNED_INT, nullptr);

    const glm::mat4 mirrorX = {
        1.f, 0.f, 0.f, 0.f,
        0.f, 1.f, 0.f, 0.f,
        0.f, 0.f, -1.f, 0.f,
        0.f, 0.f, 0.f, 1.f
    };

    gl::UniformMatrix4fv(modelLoc, 1, gl::FALSE_, glm::value_ptr(mirrorX));
    gl::DrawElements(gl::TRIANGLES, (GLsizei)indices.size(), gl::UNSIGNED_INT, nullptr);

    gl::BindVertexArray(0);

    gl::Disable(gl::DEPTH_TEST);
}

void Mesh::AppendVerts(const std::vector<glm::vec3>& other)
{
    verts.insert(verts.end(), other.begin(), other.end());
}

