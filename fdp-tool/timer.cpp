#include "timer.h"
#include <chrono>


Timer::Timer()
{
	Reset();
}

void Timer::Reset()
{
	m_prevTime = std::chrono::steady_clock::now();
}

double Timer::Tick()
{
	const auto curTime = std::chrono::steady_clock::now();
	const auto dt = std::chrono::duration<double, std::milli >(curTime - m_prevTime).count();
	m_prevTime = curTime;
	return dt;
}
