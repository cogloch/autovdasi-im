#pragma once
#include "glfw\glfw3.h"


namespace input
{
    extern bool bMousePressed[3];
    extern float mouseWheel;

    void Tick(GLFWwindow* pWindow);
	void SetCallbacks(GLFWwindow*);
}