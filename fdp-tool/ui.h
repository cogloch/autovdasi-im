#pragma once
#include "common.h"
#include <string>
#include <glm\glm.hpp>
#include "plot.h"
#include "wing.h"
#include <set>


class AirfoilMgr;
class WingMgr;
class Parser;

class UIState
{
public:
	UIState();

	void NewFrame(float dt);

	// Read-only external data 
	AirfoilMgr const * pAirfoilMgr;
	WingMgr /*const*/ * pWingMgr;
	Parser const * pParser;

	// Persistent
	bool bRankByCurrent = false;

	u32 selectedAirfoil = -1;
	u32 selectedWing = -1;

	Wing config; // Currently selected

	// One-off 
	bool bWingConfigChanged;
	bool bSaveConfigs;
	bool bSetToBestConfig;
	std::set<u32> needsUpdating;
	std::set<u32> updateDefaultSweep;

private:
	void ShowAirfoilBrowser();
	void ShowWingConfig();
	void ShowDataViz();
	void ShowConfigBrowser();

	void ResetOneoff();

private:
	Plot airfoilViz, clvsalpha, cdvsalpha, clcd3d;
};