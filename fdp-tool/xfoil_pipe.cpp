#include "xfoil_pipe.h"
#include <Windows.h>
#include <optional>


void AirfoilDataQueue::Pop(std::string & file)
{
    std::unique_lock<std::mutex> lock(mutex);
    while (files.empty())
        condVar.wait(lock);

    file = files.front();
    files.pop();
}

void AirfoilDataQueue::Push(std::string && file)
{
    std::unique_lock<std::mutex> lock(mutex);
    files.push(std::move(file));
    lock.unlock();
    condVar.notify_one();
}

struct ChildProcHandles
{
    HANDLE inWrite;
    PROCESS_INFORMATION processInfo;
    void Cleanup()
    {
        CloseHandle(processInfo.hThread);
        CloseHandle(processInfo.hProcess);
    }
};

std::optional<ChildProcHandles> OpenPipe()
{
    SECURITY_ATTRIBUTES sa = {};
    sa.nLength = sizeof(SECURITY_ATTRIBUTES);
    sa.lpSecurityDescriptor = nullptr;
    sa.bInheritHandle = true;

    HANDLE inRead, inWrite;
    if (!CreatePipe(&inRead, &inWrite, &sa, 0))
    {
        return {};
    }
    SetHandleInformation(inWrite, HANDLE_FLAG_INHERIT, 0);

    STARTUPINFO si = {};
    si.cb = sizeof(STARTUPINFO);
    si.hStdInput = inRead;
    si.hStdOutput = (HANDLE)STD_OUTPUT_HANDLE;
    si.hStdError = (HANDLE)STD_ERROR_HANDLE;
    si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_HIDE;

    PROCESS_INFORMATION processInfo;
    if (!CreateProcess(L"third_party/xfoil/xfoil.exe", nullptr, nullptr, nullptr, true, CREATE_NO_WINDOW, nullptr, L"temp/", &si, &processInfo))
    {
        CloseHandle(inRead);
        CloseHandle(inWrite);
        return {};
    }

    CloseHandle(inRead);

    return ChildProcHandles{ inWrite, processInfo };
}

void GetXfoilNACA(const std::string & code, float Re, float minAlpha, float maxAlpha, float incAlpha, AirfoilDataQueue & queue)
{
    if (code.size() != 4 && code.size() != 5)
        return;

    auto childProc = OpenPipe();
    if (!childProc)
    {
        throw "stop using exceptions";
    }

    const std::string sRe = std::to_string(Re);
    std::string file = /*"temp/" +*/ code;
    const std::string cmd = 
        "naca " + code  + "\n" + 
        "oper"          + "\n" +
        "visc"  + sRe   + "\n" + 
        //"iter"  + "30" + "\n" +  
        "pacc"          + "\n" + // Pol 
                  file  + "\n" + // Pol filename
                        + "\n" + // Dump pol 
        "aseq " + std::to_string(minAlpha) + "  " + std::to_string(maxAlpha) + " " + std::to_string(incAlpha) + "\n" + 
        //"cpwr " + "f" + "\n" + // Cp
                          "\n" +   // Go up to main menu
        "quit";

    WriteFile(childProc->inWrite, cmd.c_str(), cmd.size(), nullptr, nullptr);

    CloseHandle(childProc->inWrite);
    WaitForSingleObject(childProc->processInfo.hThread, INFINITE);
    childProc->Cleanup();

    queue.Push(std::move(file));
}

void BatchXfoilNACA(const std::vector<std::string> & codes, float Re, float minAlpha, float maxAlpha, float incAlpha, AirfoilDataQueue & queue)
{
    // TODO fix everything 

    auto childProc = OpenPipe();
    if (!childProc)
    {
        throw "stop using exceptions";
    }

    std::string cmd = "naca 0015\nvisc " + std::to_string(Re) + "\n\n"; // Dummy to set Re
    for (auto & code : codes)
    {
        if (code.size() != 4 && code.size() != 5)
            continue;

        cmd +=
            "naca " + code + "\n" +
            "oper"         + "\n" + 
            "pacc"         + "\n" + // Enable polar accum
                      code + "\n" + // Polar
                             "\n" + // Polar dump 
            "aseq " + std::to_string(minAlpha) + "  " + std::to_string(maxAlpha) + " " + std::to_string(incAlpha) + "\n" +
            "pacc"         + "\n" + // Disable polar accum
                             "\n"; // Go up to main menu 
    }
    cmd += "quit\n";

    WriteFile(childProc->inWrite, cmd.c_str(), cmd.size(), nullptr, nullptr);

    CloseHandle(childProc->inWrite);
    WaitForSingleObject(childProc->processInfo.hThread, INFINITE);
    childProc->Cleanup();

    // TODO ditch queue, just use plain mutex
    for (auto & code : codes)
    {
        std::string temp = code;
        queue.Push(std::move(temp));
    }
}
