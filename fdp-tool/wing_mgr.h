#pragma once
#include <mutex>
#include <vector>
#include <atomic>
#include <rapidjson/document.h>
#include "wing.h"


class WingMgr
{
public:
    WingMgr();
    ~WingMgr();

    void Sync(AirfoilContainer, u32 airfoilIdOffset);
    void StartUpdating();
    void StopUpdating();
    bool IsUpdating() const;

    Wing& GetWingRef(u32 rank);
	const Wing& GetWingConstRef(u32 rank) const;
    Wing* GetWingPtr(u32 rank);
	const Wing* GetWingConstPtr(u32 rank) const;
    size_t GetNumWings() const;

    void SaveWings();
    void RankWings(bool bRankByCurrent = false);

	std::vector<u32> ranking;
	std::vector<u32> rankingBuff;
	
private:
    void LoadSavedConfigs();

    void UpdateThread();

    std::thread m_updateThread;
    std::atomic<bool> m_bUpdating;
    std::mutex m_bufferMtx;
    std::vector<Wing> m_updateBuffer;

    void GenerateAllValidConfigs();
    AirfoilContainer m_airfoilsCpy;
    u32 m_airfoilIdOffset;

	friend class Wing;
    std::vector<Wing> m_wings;
	std::vector<Wing> m_bestWings;

    void LoadWingConfig(rapidjson::Value& serialized);
    bool WingExists(u32 id) const;
    bool IsDuplicateConfig(const Wing&) const;
};