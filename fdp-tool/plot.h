#pragma once
#include <imgui/imgui.h>
#include <glm/glm.hpp>
#include <vector>
#include "common.h"
#include <string>


struct Plot
{
    std::string parentWindow;
    float minWidth = 100.f, minHeight = 100.f;

    ImColor bgColor = { 50, 50, 50 };
    ImColor borderColor = { 255, 255, 255 };

    u32 numGridLines = 30;
    ImColor axesColor = { 0, 0, 0 };
    ImColor gridlinesColor = { 0, 0, 0, 127 };

    ImColor dataColor = { 255, 255, 0 };
    float dataThickness = 2.f; // TODO non-shit name 

    bool bAllowStretching = true;

    void LinePlot(std::vector<glm::vec2> points); // Copy since all points will be mapped to a different coord system for plotting 

private:
    void DrawBg(ImDrawList*, const ImVec2 canvasPos, const ImVec2 canvasSz) const;
};