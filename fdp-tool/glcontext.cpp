#include "glcontext.h"
#include <gl/glcore.hpp>
#include <gl/wgl.hpp>
#include <string>


bool GLContext::s_bLoadedFunctions = false;

GLContext::GLContext()
    : m_hWindow(nullptr)
    , m_hDeviceContext(nullptr)
    , m_hRenderContext(nullptr)
{
}

GLContext::~GLContext()
{
    DestroyContext();
}

void GLContext::DestroyContext()
{
    wglMakeCurrent(m_hDeviceContext, nullptr);
    wglDeleteContext(m_hRenderContext);
    ReleaseDC(m_hWindow, m_hDeviceContext);
}

void GLContext::Init(HWND hWindow, u32 width, u32 height, u32 majorVersion, u32 minorVersion)
{
    m_hDeviceContext = GetDC(hWindow);

    CreatePixelFormat();
    CreateContext(majorVersion, minorVersion);

    gl::ClearColor(0.4f, 0.6f, 0.9f, 1.0f);
    gl::Viewport(0, 0, width, height);
}

void GLContext::CreatePixelFormat()
{
    PIXELFORMATDESCRIPTOR pixelFormatDesc;
    memset(&pixelFormatDesc, 0, sizeof(PIXELFORMATDESCRIPTOR));
    pixelFormatDesc.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
    pixelFormatDesc.iPixelType = PFD_TYPE_RGBA;
    pixelFormatDesc.cColorBits = 32;
    pixelFormatDesc.cDepthBits = 32;
    pixelFormatDesc.iLayerType = PFD_MAIN_PLANE;

    auto pixelFormat = ChoosePixelFormat(m_hDeviceContext, &pixelFormatDesc);
    SetPixelFormat(m_hDeviceContext, pixelFormat, &pixelFormatDesc);
}

void GLContext::CreateContext(u32 majorVersion, u32 minorVersion)
{
    // Create a temporary Opengl 2.1 context to allow the loading of functions that can create a 4.5 context
    auto tempContext = wglCreateContext(m_hDeviceContext);
    wglMakeCurrent(m_hDeviceContext, tempContext);

    if (!s_bLoadedFunctions)
    {
        LoadFunctions();
    }

    if (!s_bLoadedFunctions)
    {
        // TODO: Decide if crash or allow using the 2.1 context
        // DestroyContext();
        m_hRenderContext = tempContext;
        return;
    }

    // If the context creation extension is available, create an Opengl majorVersion.minorVersion context and make it active(replacing the temporary one)
    // If not, keep using the 2.1 context
    if (wgl::exts::var_ARB_create_context)
    {
        int attributes[] = {
            wgl::CONTEXT_MAJOR_VERSION_ARB, majorVersion,
            wgl::CONTEXT_MINOR_VERSION_ARB, minorVersion,
            wgl::CONTEXT_FLAGS_ARB, wgl::CONTEXT_CORE_PROFILE_BIT_ARB,
            0
        };

        m_hRenderContext = wgl::CreateContextAttribsARB(m_hDeviceContext, nullptr, attributes);

        wglDeleteContext(tempContext);
        wglMakeCurrent(m_hDeviceContext, m_hRenderContext);
    }
    else
    {
        // TODO: Decide if crash or allow using the 2.1 context
        // DestroyContext();
        m_hRenderContext = tempContext;
    }

}

void GLContext::LoadFunctions() const
{
    auto glLoad = gl::sys::LoadFunctions();
    if (!glLoad)
        throw "Failed loading gl functions.\n";        

    auto wglLoad = wgl::sys::LoadFunctions(m_hDeviceContext);
    if (!wglLoad)
        throw "Failed loading wgl functions.\n";

    s_bLoadedFunctions = true;
}

void GLContext::SwapContextBuffers() const
{
    SwapBuffers(m_hDeviceContext);
}

void GLContext::ClearBackBuffer()
{
    gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT | gl::STENCIL_BUFFER_BIT);
}

void GLContext::ClearBackBuffer(float r, float g, float b, float a)
{
    gl::ClearColor(r, g, b, a);
    ClearBackBuffer();
}

void GLContext::Resize(u32 width, u32 height)
{
    gl::Viewport(0, 0, width, height);
}