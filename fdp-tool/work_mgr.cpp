#include "work_mgr.h"


WorkMgr::WorkMgr()
{
    numHwThreads = std::thread::hardware_concurrency();

    // TODO temp
    // Num threads in pool = <> - 2 for parsing thread and config generation thread 
    //threadPool.resize(numHwThreads - 2);
}

WorkMgr::~WorkMgr()
{
    for (auto& thread : threadPool)
        thread.join();
}
