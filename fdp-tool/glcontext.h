#pragma once
#include "common.h"
#include <windows.h>


class GLContext
{
public:
    GLContext();
    ~GLContext();

    void Init(HWND, u32 width, u32 height, u32 majorVersion = 3, u32 minorVersion = 3);

    // TODO: Add clear flags
    static void ClearBackBuffer();
    static void ClearBackBuffer(float r, float g, float b, float a);
    void SwapContextBuffers() const;

    static void Resize(u32 width, u32 height);

private:
    HWND  m_hWindow;
    HDC   m_hDeviceContext;
    HGLRC m_hRenderContext;

    static bool s_bLoadedFunctions;
    // Returns false on failure.
    void LoadFunctions() const;

private:
    void CreatePixelFormat();
    void CreateContext(u32 majorVersion, u32 minorVersion);
    void DestroyContext();
};