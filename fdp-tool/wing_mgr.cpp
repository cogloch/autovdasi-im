#include "wing_mgr.h"
#include <string>
#include <fstream>
#include <algorithm>


void WingMgr::RankWings(bool bRankByCurrent)
{
    if(bRankByCurrent)
    {
		std::sort(ranking.begin(), ranking.end(), [&wings = m_wings](u32 a, u32 b) {
			return wings[a].GetMaxClCd() > wings[b].GetMaxClCd();
		});
        return;
    }
    
    // Rank by max cdcl for user-set configs for now 
	std::sort(ranking.begin(), ranking.end(), [&wings = m_bestWings](u32 a, u32 b) {
		return wings[a].GetMaxClCd() > wings[b].GetMaxClCd();
	});
}

bool WingMgr::WingExists(u32 id) const
{
    return id < m_wings.size();
}

bool WingMgr::IsDuplicateConfig(const Wing& wing) const
{
	// TODO fix this

    for (const auto& existing : m_wings)
        if (existing == wing)
            return true;

	for (const auto& existing : m_updateBuffer)
		if (existing == wing)
			return true;

    return false;
}

Wing& WingMgr::GetWingRef(u32 id)
{
    return m_wings[ranking[id]];
}

const Wing & WingMgr::GetWingConstRef(u32 id) const
{
	return m_wings[ranking[id]];
}

Wing* WingMgr::GetWingPtr(u32 id)
{
    return &m_wings[ranking[id]];
}

const Wing * WingMgr::GetWingConstPtr(u32 id) const
{
	return &m_wings[ranking[id]];
}

size_t WingMgr::GetNumWings() const
{
    return m_wings.size();
}

WingMgr::WingMgr()
{
    LoadSavedConfigs();
}

WingMgr::~WingMgr()
{
    StopUpdating();

    if (m_updateThread.joinable())
        m_updateThread.join();
}

void WingMgr::GenerateAllValidConfigs()
{
    // The current batch 
    const auto numAirfoils = m_airfoilsCpy.meta.size();
    if (!numAirfoils) 
        return;

	//for (u32 i = 0; i < numAirfoils; ++i)
    for (u32 i = m_airfoilIdOffset; i < numAirfoils; ++i)
    {
        // Airfoil 1
        const auto numCoords1 = m_airfoilsCpy.geom[i].coords.size();
        if (!numCoords1) continue;
        
        // TODO: temp fix 

        Wing newConfig(i, i, m_wings.size() + m_updateBuffer.size());
        newConfig.rootMeta = m_airfoilsCpy.meta[i];
        newConfig.tipMeta = m_airfoilsCpy.meta[i];
        if (IsDuplicateConfig(newConfig))
        {
            continue;
        }

        m_bestWings.push_back(newConfig);
        rankingBuff.push_back(newConfig.GetId());
        newConfig.CalculatePerformance(m_airfoilsCpy.data[i], m_airfoilsCpy.data[i], m_airfoilsCpy.geom[i], m_airfoilsCpy.geom[i], m_airfoilsCpy.meta[i], m_airfoilsCpy.meta[i]);

        m_updateBuffer.push_back(std::move(newConfig));

   //     for (u32 j = i; j < numAirfoils; ++j)
   //     {
   //         // Airfoil 2
   //         const auto numCoords2 = m_airfoilsCpy.geom[j].coords.size();
   //         if (!numCoords2) continue;
   //         if (numCoords1 != numCoords2) continue;

   //         //Wing newConfig(i + m_airfoilIdOffset, j + m_airfoilIdOffset, m_wings.size() + m_updateBuffer.size());
			//Wing newConfig(i, j, m_wings.size() + m_updateBuffer.size());
			//newConfig.rootMeta = m_airfoilsCpy.meta[i];
			//newConfig.tipMeta = m_airfoilsCpy.meta[j];
			//if (IsDuplicateConfig(newConfig))
			//{
			//	continue;
			//}

			//m_bestWings.push_back(newConfig);
			//rankingBuff.push_back(newConfig.GetId());
   //         newConfig.CalculatePerformance(m_airfoilsCpy.data[i], m_airfoilsCpy.data[j], m_airfoilsCpy.geom[i], m_airfoilsCpy.geom[j], m_airfoilsCpy.meta[j], m_airfoilsCpy.meta[i]);

   //         m_updateBuffer.push_back(std::move(newConfig));
   //     }
    }
}

void WingMgr::Sync(AirfoilContainer airfoils, u32 airfoilIdOffset)
{
    std::lock_guard<std::mutex> lock(m_bufferMtx);
    if (airfoils.data.empty()) return;

	m_airfoilsCpy.Reset();

    auto it = std::next(m_updateBuffer.begin(), m_updateBuffer.size());
    std::move(m_updateBuffer.begin(), it, std::back_inserter(m_wings));
    m_updateBuffer.erase(m_updateBuffer.begin(), it);

	auto itRank = std::next(rankingBuff.begin(), rankingBuff.size());
	std::move(rankingBuff.begin(), itRank, std::back_inserter(ranking));
	rankingBuff.erase(rankingBuff.begin(), itRank);

    /*m_airfoilsCpy.meta.insert(m_airfoilsCpy.meta.end(), airfoils.meta.begin(), airfoils.meta.end());
	m_airfoilsCpy.data.insert(m_airfoilsCpy.data.end(), airfoils.data.begin(), airfoils.data.end());*/
    m_airfoilIdOffset = airfoilIdOffset;
	m_airfoilsCpy = airfoils;
}

void WingMgr::StartUpdating()
{
    m_bUpdating = true;
    m_updateThread = std::thread(&WingMgr::UpdateThread, this);
}

void WingMgr::StopUpdating()
{
    m_bUpdating = false;
}

bool WingMgr::IsUpdating() const
{
    return m_bUpdating;
}

void WingMgr::UpdateThread()
{
    while (m_bUpdating)
    {
        std::lock_guard<std::mutex> lock(m_bufferMtx);
        GenerateAllValidConfigs();
    }

    StopUpdating();
}

void WingMgr::LoadWingConfig(rapidjson::Value& serialized)
{
    m_wings.push_back(Wing(serialized["rootAirfoil"].GetInt(), serialized["tipAirfoil"].GetInt(), m_wings.size()));
    auto& wing = m_wings.back();
	m_bestWings.push_back(Wing(wing));
	wing.Deserialize(serialized);
}

void WingMgr::LoadSavedConfigs()
{
    std::string line, data;
    std::ifstream file("wings.json");
    while (std::getline(file, line))
        data += line + "\n";

    if (data.empty())
        return;

    rapidjson::Document root;
    root.Parse(data.c_str());

    rapidjson::Value& wings = root;
    for (rapidjson::SizeType i = 0; i < wings.Size(); i++)
        LoadWingConfig(wings[i]);
}

void WingMgr::SaveWings()
{
    rapidjson::StringBuffer sb;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(sb);

    writer.StartArray();
    for (const auto& wing : m_wings)
        wing.Serialize(writer);
    writer.EndArray();

    std::ofstream file("wings.json");
    file << sb.GetString();
}