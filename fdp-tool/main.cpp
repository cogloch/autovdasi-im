﻿#include "app.h"
#include <string>
#include <iostream>

#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "glfw3-86.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "LibXlsxWriter")


int main()
{
    try
    {
        App app;
        app.RunMainLoop();
    }
    catch (const std::string & err)
    {
        std::cout << err;
        getchar();
    }
}