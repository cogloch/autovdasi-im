#pragma once
#include <glm\glm.hpp>


struct PointLight
{
    PointLight(const glm::vec3& pos_, const glm::vec3 color_) 
        : pos(pos_)
        , color(color_)
    {}

    glm::vec3 pos;
    glm::vec3 color;
};