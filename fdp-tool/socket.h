#pragma once
#include <winsock2.h>
#include <string>


namespace sock
{
    extern WSADATA wsaData;
    extern SOCKET socket;
    extern std::string hostname;

    void Init();
    void Cleanup();
    void Connect(const std::string& url);
    std::string HTTP_GET(const std::string& path);
}