#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <gl/glcore.hpp>
#include "light.h"
#include "camera.h"
#include "common.h"


struct Mesh
{
    GLuint vao, vbo, ebo;
    bool bRenderable = false;

    static GLuint shaderHandle;
    static GLint modelLoc, viewLoc, projLoc;
    static void InitCommonResources();

    struct Vertex
    {
        Vertex(const glm::vec3& pos = { 0,0,0 });
        glm::vec3 pos;
        glm::vec3 normal;
    };

    std::vector<Vertex> verts;
    std::vector<u32> indices;

    glm::mat4 xform;

    void CreateGraphicsResources();
    void GenerateNormals();
    void SetQuadFace(u32 v0, u32 v1, u32 v2, u32 v3);

    void Render(const Camera&, const PointLight&);

    void AppendVerts(const std::vector<glm::vec3>&);
};